<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $connection = 'sent_mail';
    protected $table = 'email';
    protected $dates = ['birth_day', 'created_at', 'updated_at'];
    protected $fillable = ['email_address', 'full_name', 'birth_day', 'gender', 'phone', 'address', 'province_id'];

    public function setBirthDayAttribute($value)
    {
        $day=substr($value,0,2);
        $month=substr($value,3,2);
        $year=substr($value,6,4);
        $result=$year.'-'.$month.'-'.$day;
        $this->attributes['birth_day'] = $result;
    }
}
