<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\Contracts\ProvinceRepository::class, \App\Repositories\Eloquents\ProvinceRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\JobCarrerRepository::class, \App\Repositories\Eloquents\JobCarrerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\IndustrialZoneRepository::class, \App\Repositories\Eloquents\IndustrialZoneRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\JobListRepository::class, \App\Repositories\Eloquents\JobListRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\EmployerRepository::class, \App\Repositories\Eloquents\EmployerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\JobWageRepository::class, \App\Repositories\Eloquents\JobWageRepositoryEloquent::class);
        //:end-bindings:
    }
}
