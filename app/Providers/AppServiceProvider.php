<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    	 view()->composer('*', function ($view)
        {
            $request = new Request();
            $remote = $request->ajax() ? true : null;
            $layout = $remote ? 'layouts.ajax' : 'layouts.html';
            $view->with(compact('layout','remote'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RepositoryServiceProvider::class);
    }
}
