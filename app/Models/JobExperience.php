<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class JobExperience extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [];
    protected $table = 'job_experience';

    public function jobLists()
    {
        return $this->hasMany('App\Models\JobList', 'job_experience');
    }

}
