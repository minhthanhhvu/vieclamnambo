<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobLevel extends Model
{
    protected $table = 'job_level';

    public function jobList()
    {
        return $this->hasMany('App\Models\JobList', 'job_level');
    }
}
