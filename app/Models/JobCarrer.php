<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class JobCarrer extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [];
    protected $table = 'job_carrer';

    public function jobLists()
    {
        return $this->belongsToMany('App\Models\JobList', 'job_carrer_relation',  'carrer_id', 'job_id')->orderBy('updated_at', 'desc');
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeOrderByRanking($query)
    {
        return $query->orderBy('ranking', 'asc');
    }

    public function scopeOther($query, $carrer_id)
        
    {
        return $query->where('job_carrer.id', '<>', $carrer_id);
    }
}
