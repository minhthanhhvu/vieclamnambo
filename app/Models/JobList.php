<?php

namespace App\Models;

use DB;
use App\Models\JobBenefit;
use App\Helpers\Date;
use Illuminate\Database\Eloquent\Model;
use Debugbar;

class JobList extends Model
{
    protected $fillable = [];
    protected $table = 'job_list';
    protected $dates = ['job_ended_at', 'created_at', 'updated_at'];

    public function jobWage()
    {
        return $this->belongsTo('App\Models\JobWage', 'job_wage');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }

    public function industrialZone()
    {
        return $this->belongsTo('App\Models\IndustrialZone', 'industrial_id');
    }

    public function jobExperience()
    {
        return $this->belongsTo('App\Models\JobExperience', 'job_experience');
    }

    public function jobCarrers()
    {
        return $this->belongsToMany('App\Models\JobCarrer', 'job_carrer_relation', 'job_id', 'carrer_id');
    }

    public function jobLevel()
    {
        return $this->belongsTo('App\Models\JobLevel', 'job_level');
    }

    public function customSaveJobs()
    {
        return $this->hasMany('App\Models\CustomSaveJob', 'custom_id');
    }

    // Lấy ra 'job_benefit' dạng mảng
    public function getJobBenefitAttribute($value)
    {
        $result = array();
        $benefits = explode(',', $value);
        foreach ($benefits as $benefit){
            $result[] = JobBenefit::find($benefit);
        }
        if(empty($result))
            $result = null;
        return $result;
    }

    //Lấy ra trường Hạn nộp hồ sơ 'job_end_at'
    public function getJobEndedAtAttribute($value)
    {
        switch ($this->attributes['expired']){
            case 0:
                $result = 'Hết hạn';
            case 1:
                //Nếu việc làm còn hạn
                if ($value > date('Y-m-d')){
                    $result = Date::getDate($value);
                }else{
                    $result = 'Hết hạn';
                }
            case 2:
                $result = 'Tuyển thường xuyên';
            case 3:
                $result = 'Tuyển đến khi đủ số lượng';
            default:
                //Nếu việc làm còn hạn
                if ($value > date('Y-m-d')){
                    $result = Date::getDate($value);
                }else{
                    $result = 'Hết hạn';
                }
        }
        return $result;
    }

    //Hiển thị việc làm còn hạn bao nhiêu ngày
    public function subJobEndedAt()
    {
        $job_ended_at = $this->job_ended_at;
        if ($job_ended_at == '0000:00:00' || $job_ended_at < date('Y-m-d')){
            $result = '0';
        }else{
            $result = mktime(0, 0, 0, date('m') - date('m', strtotime($job_ended_at)), date('d') - date('d', strtotime($job_ended_at)), date('N') - date('N', strtotime($job_ended_at)));
            $result = date('d', $result);
        }
        return $result;
    }

    public function checkNew()
    {
        $updated_at = $this->updated_at;
        if (date('Y-m-d') == date('Y-m-d', strtotime('-1 day', strtotime($updated_at)))){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }

    public function checkHot()
    {
        $result = '';

        //Kiểm tra việc làm mới
        $updated_at = $this->attributes['updated_at'];
        if (date('Y-m-d') == date('Y-m-d', strtotime('-1 day', strtotime($updated_at)))){
            $result .= '<span class="label label-danger">MỚI</span>';
        }

        //Kiểm tra việc làm tuyển gấp
        $end = strtotime(date('Y-m-d'));
        $start = strtotime("-14 day", $end);
        $job_end_at = $this->attributes['job_ended_at'];
        if ($job_end_at > date('Y-m-d', $start) && $job_end_at < date('Y-m-d', $end)){
            $result .='<span class="label label-danger">GẤP</span>';
        }

        $str = 'Start = '.date('Y-m-d', $start).' | End = '.$job_end_at < date('Y-m-d', $end).' | job_end_at = '.$job_end_at;
        Debugbar::info($job_end_at);

        //Kiểm tra việc làm HOT (lương cao)

        return $result;
    }

    //Lấy ra việc làm được được kích hoạt
    public function scopeActive($query)
    {
        return $query
            ->where('is_active', 1)
            ->whereExists(function ($query){
                $query->select(DB::raw(1))
                    ->from('companies')
                    ->whereRaw('job_list.company_id = companies.id');
            });
    }

    //Sắp xếp việc làm theo ngày cập nhật giảm dần
    public function scopeLatest($query)
    {
        return $query->orderBy('updated_at', 'desc');
    }

    //Lấy ra việc làm sắp hết hạn (dưới 14 ngày)
    public function scopeExpiredSoon($query)
    {
        $end = strtotime(date('Y-m-d'));
        $start = strtotime("-14 day", $end);
        $query->where('expired', 1)->where('job_ended_at', '>', date('Y-m-d', $start))->where('job_ended_at', '<', date('Y-m-d', $end));
    }

    //Lấy ra việc làm còn hạn
    public function scopeNotExpired($query)
    {
        $query->where('job_ended_at', '>', date('Y-m-d'));
    }

    //Sắp xếp việc làm theo hạn nộp hồ sơ giảm dần
    public function scopeOrderByEndedAt($query)
    {
        return $query->orderBy('job_ended_at', 'desc');
    }
}
