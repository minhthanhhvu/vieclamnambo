<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class IndustrialZone extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [];
    protected $table = 'industrial_zones';

    public function jobLists()
    {
        return $this->hasMany('App\Models\JobList', 'industrial_id');
    }

    public function companies()
    {
        return $this->hasMany('App\Models\Company', 'industrial_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id');
    }

    public function getLocationAttribute($value)
    {
        if ($value) {
            $result = explode(', ', $value);
        }else{
            $result = null;
        }
        return $result;
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }
}
