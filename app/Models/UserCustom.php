<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Helpers\Date;

class UserCustom extends Authenticatable
{
    use Notifiable;

    protected $table = 'user_custom';
    protected $dates = ['birthday', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'avatar', 'fullname', 'gender', 'province', 'address', 'birthday', 'phone', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'old_password'
    ];

    public function customSaveJobs()
    {
        return $this->hasMany('App\Models\CustomSaveJob', 'custom_id');
    }

    public function getBirthdayAttribute($value)
    {
        if ($value == '0000-00-00'){
            $result = null;
        }else{
            $result = Date::getDate($value);
        }
        return $result;
    }

    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = Date::setDate($value);
    }
    
    /*
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = md5($value);
    }
    */
}
