<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class JobWage extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'job_wage';
    protected $fillable = [];

    public function jobLists()
    {
        return $this->hasMany('App\Models\JobList', 'job_wage');
    }
}
