<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomSaveJob extends Model
{
    protected $table = 'custom_save_job';
    
    public function userCustom()
    {
        return $this->belongsTo('App\Models\UserCustom', 'custom_id');
    }

    public function jobList()
    {
        return $this->belongsTo('App\Models\JobList', 'job_id');
    }
}
