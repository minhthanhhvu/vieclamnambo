<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $fillable = ['user_id', 'provider_user_id', 'provider_id'];

    public function userCustom()
    {
        return $this->belongsTo('App\Models\UserCustom');
    }
}
