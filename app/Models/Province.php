<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'province';

    protected $fillable = [''];

    public function jobLists()
    {
        return $this->hasMany('App\Models\JobList')->orderBy('updated_at', 'desc');
    }

    public function company()
    {
        return $this->hasMany('App\Models\Company');
    }

    public function industrialZones()
    {
        return $this->hasMany('App\Models\IndustrialZone')->orderBy('ranking', 'asc');
    }
    
    public function getImagesAttribute($value)
    {
        if ($value){
            $result = explode(',', $value);
        }else{
            $result = null;
        }
        return $result;
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }
}
