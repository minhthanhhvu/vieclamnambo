<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Company extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'companies';
    protected $fillable = [];

    public function jobLists(){
        return $this->hasMany('App\Models\JobList');
    }

    public function industrialZones()
    {
        return $this->belongsToMany('App\Models\IndustrialZone', 'company_industrial_relation', 'company_id', 'industrial_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }
}
