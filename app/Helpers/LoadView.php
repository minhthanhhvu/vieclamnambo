<?php
namespace App\Helpers;

use App\Models\Province;
use App\Models\IndustrialZone;
use App\Models\Page;

class LoadView
{
    public static function loadMenu()
    {
        $result = '';
        $provinces = Province::select('id', 'name', 'slug')->where('is_active', 1)->orderBy('ranking', 'asc')->get();
        foreach ($provinces as $province){
            $result .= '<li><a href="/'.$province->slug.'">'.$province->name.'<span class="caret"></span></a>';
            $industrial_zones = IndustrialZone::where('province_id', $province->id)->select('name', 'slug')->orderBy('ranking', 'asc')->get();
            if ($industrial_zones->count() > 0){
                $result .= '<ul class="list-unstyled">';
                foreach ($industrial_zones as $industrial_zone){
                    $result .= '<li><a href="/'.$province->slug.'/'.$industrial_zone->slug.'">'.$industrial_zone->name.'</a></li>';
                }
                $result .= '</ul>';
            }
            $result .= '</li>';
        }
        $result .= '';
        return $result;

        /*
        $result = '';
        $provinces = Province::with(['industrialZones' =>
            function($query){
                $query->select('name', 'slug')->orderBy('ranking', 'asc');
            }])
            ->select('name', 'slug')
            ->where('is_active', 1)
            ->orderBy('ranking', 'asc')
            ->get();
        foreach ($provinces as $province){
            $result .= '<li><a href="/'.$province->slug.'">'.$province->name.'<span class="caret"></span></a>';
            $industrial_zones = $province->industrialZones;
            //dd($industrial_zones);
            if ($industrial_zones->count() > 0){
                $result .= '<ul class="list-unstyled">';
                foreach ($industrial_zones as $industrial_zone){
                    $result .= '<li><a href="/'.$province->slug.'/'.$industrial_zone->slug.'">'.$industrial_zone->name.'</a></li>';
                }
                $result .= '</ul>';
            }
            $result .= '</li>';
        }
        return $result;
        */
    }

    public static function pages()
    {
        $pages = Page::select('name', 'slug')->where('is_active', 1)->orderBy('updated_at', 'desc')->get();
        return $pages;
    }

    public static function industrialZones($province_id)
    {
        $result = Province::find(old('province_id'))->industrialZones()->orderBy('ranking', 'asc')->get();
        return $result;
    }
}