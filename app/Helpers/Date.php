<?php
namespace App\Helpers;

class Date
{
    public static function setDate($value)
    {
        $day=substr($value,0,2);
        $month=substr($value,3,2);
        $year=substr($value,6,4);
        if($day && $month && $year) {
            $result = $year . '-' . $month . '-' . $day;
        }else{
            $result = null;
        }
        return $result;
    }

    public static function getDate($value)
    {
        $day=substr($value,0,4);
        $month=substr($value,5,2);
        $year=substr($value,8,2);
        if($day && $month && $year) {
            $result = $year . '/' . $month . '/' . $day;
        }else{
            $result = null;
        }
        return $result;
    }
}