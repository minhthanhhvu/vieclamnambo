<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscribeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email_address' => 'required|email|unique:sent_mail.email,email_address',
            'g-recaptcha-response' => 'required|recaptcha',
        ];
    }

    public function messages()
    {
        return [
            'email_address.unique'=>'Email này đã tồn tại',
            'g-recaptcha-response.required' => 'Bạn chưa xác nhận không phải robot',
            'g-recaptcha-response.recaptcha' => 'Xác nhận sai',
        ];
    }
}