<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Auth;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required|min:6',
            'gender' => 'required',
            'province' => 'required',
            'address' => 'required',
            'birthday' => 'required|date_format:d/m/Y',
            'phone' => 'required|numeric|min:10',
            'email' => [
                'required',
                'email',
                Rule::unique('user_custom')->ignore(Auth::user()->id),
             ],
        ];
    }

    public function messages()
    {
        return [
            'fullname.required' => 'Chưa nhập họ tên',
            'gender.required' => 'Chưa chọn giới tính',
            'province.required' => 'Chưa chọn tỉnh / thành phố mong muốn làm việc',
            'address.required' => 'Chưa nhập địa chỉ',
            'birthday.required' => 'Chưa chọn ngày sinh',

            'phone.required' => 'Chưa nhập số điện thoại',
            'phone.numberic' => 'Số điện thoại phải là số',
            'phone.min' => 'Số điện thoại phải trên :min số',

            'email.required' => 'Chưa nhập email',
            'email.email' => 'Định dạng email không hợp lệ',
            'email.unique' => 'Email này đã tồn tại'
        ];
    }
}
