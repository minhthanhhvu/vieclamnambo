<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
        if(!Auth::check() or $request->user()->role!='admin') {
            return redirect('auth/login');
        }
        */
        if(!Auth::check()) {
            return redirect('auth/login');
        }
        return $next($request);
    }
}
