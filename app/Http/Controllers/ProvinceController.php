<?php

namespace App\Http\Controllers;

use App\Models\JobList;
use App\Models\Province;

class ProvinceController extends Controller
{
    //Việc làm theo Tỉnh thành
    public function index($slug1){
        $province = Province::with('jobLists')->where('slug', $slug1)->active()->first();
        
        //Kiểm tra URL tỉnh thành có tồn tại hay không
        if($province){
            //Lấy ra danh sách việc làm theo tỉnh thành (có phân trang)
            $job_lists = $province->jobLists()->active()->latest()->paginate(10);

            //Lấy ra 5 việc làm gấp (sắp hết hạn)
            $expired_job_lists = $province->jobLists()->active()->expiredSoon()->orderByEndedAt()->take(5)->get();

            //Lấy ra 5 việc làm hấp dẫn (lương cao nhất)
            $high_job_lists =  $province->jobLists()->active()->latest()->take(5)->get();

            return view('province',compact('job_lists', 'province', 'high_job_lists', 'expired_job_lists'));
        }else{
            return view('errors.404');
        }
    }
}
