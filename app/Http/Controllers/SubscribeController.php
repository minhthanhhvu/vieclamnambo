<?php

namespace App\Http\Controllers;

use App\Email;
use App\Http\Requests\SubscribeRequest;

class SubscribeController extends Controller
{

    public function create()
    {
    	return view('subscribe.create');
    }

    public function store(SubscribeRequest $request)
    {
        $email = Email::create($request->all());
        session()->flash('flash_message', 'Xác nhận đăng ký thành công! Việc Làm Nam Bộ sẽ cập nhật đến bạn những tin tức việc làm mới nhất mỗi ngày.');
    	return redirect('dang-ky/thong-bao.html');
    }

    public function success()
    {
        return view('subscribe.success');
    }
}
