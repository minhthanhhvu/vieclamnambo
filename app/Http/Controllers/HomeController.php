<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JobCarrer;
use App\Models\JobList;
use App\Models\Province;
use App\Models\Company;
use App\Models\IndustrialZone;
use App\Models\JobWage;
use Debugbar;

class HomeController extends Controller
{

    public function index()
    {
        //Lấy ra dữ liệu đưa vào ListBox tìm kiếm
        $listbox_carrers = JobCarrer::active()->select('name', 'id')->where('is_active', 1)->orderBy('ranking', 'asc')->get();
        $listbox_provinces = Province::active()->select('name', 'id')->orderBy('ranking', 'asc')->get();

        //Lấy ra ngành nghề và việc làm theo ngành nghề đó
        $job_carrers = JobCarrer::with(['jobLists' => function($query){
                $end = strtotime(date('Y-m-d H:i:s'));
                $start = strtotime("-14 day", $end);
                $query->where('job_list.updated_at', '>', date('Y-m-d H:i:s', $start))->where('job_list.updated_at', '<', date('Y-m-d H:i:s', $end));
            }])
            ->orderBy('ranking', 'asc')
            ->take(8)
            ->get();

        //Lấy ra thông báo việc làm mới trong tỉnh thành và khu công nghiệp
        $new_job_provinces = Province::with(['industrialZones.jobLists' => function($query){
                $end = strtotime(date('Y-m-d H:i:s'));
                $start = strtotime("-14 day", $end);
                $query->where('job_list.updated_at', '>', date('Y-m-d H:i:s', $start))->where('job_list.updated_at', '<', date('Y-m-d H:i:s', $end));
            }])
            ->where('is_active', 1)
            ->select('name', 'slug')
            ->orderBy('updated_at', 'desc')
            ->get();

        //Lấy ra việc làm mới nhất
        $new_job_lists = JobList::with('company', 'province', 'jobWage', 'industrialZone')->active()->where('job_ended_at', '>', date('Y-m-d H:i:s'))->latest()->take(50)->get();

        //Lấy việc làm hấp dẫn
        $high_job_lists = JobList::with('company', 'province', 'jobWage', 'industrialZone')->active()->NotExpired()->latest()->take(50)->get();

        //Lấy việc làm nổi bật
        $hot_job = JobList::with('jobWage', 'jobExperience', 'industrialZone')->active()->NotExpired()->latest()->first();

        //Việc làm tuyển gấp
        $recuit_job = JobList::with('jobWage', 'jobExperience', 'industrialZone')->active()->NotExpired()->latest()->first();
        
        return view('home', compact('employers', 'job_carrers', 'new_job_lists', 'high_job_lists', 'provinces', 'listbox_carrers', 'listbox_provinces', 'listbox_industrial_zones', 'recuit_job', 'hot_job', 'new_job_provinces'));
    }
}