<?php

namespace App\Http\Controllers;

use App\Models\JobCarrer;
use App\Models\JobList;
use Illuminate\Http\Request;

class CarrerController extends Controller
{
    //Việc làm theo Ngành nghề
    public function index($slug1)
    {
        $job_carrer = JobCarrer::with('jobLists')->active()->where('slug', $slug1)->first();

        //Kiểm tra URL tỉnh thành có tồn tại hay không
        if($job_carrer){
            //Lấy ra danh sách việc làm theo ngành nghề (có phân trang)
            $job_lists = $job_carrer->jobLists()->with( 'province', 'company', 'industrialZone', 'jobWage')->active()->latest()->paginate(10);

            //Lấy ra 5 việc làm gấp (sắp hết hạn)
            $expired_jobs = $job_carrer->jobLists()->with( 'province', 'company', 'industrialZone', 'jobWage')->active()->expiredSoon()->orderByEndedAt()->take(5)->get();

            //Lấy ra 5 việc làm hấp dẫn (lương cao nhất)
            $high_job_list = $job_carrer->jobLists()->active()->latest()->take(5)->get();

            //Việc làm nổi bật
            $hot_job = $job_carrer->jobLists()->active()->latest()->first();

            //Lấy ra ngành nghề khác
            $other_carrers = JobCarrer::active()->other($job_carrer->id)->orderByRanking()->get();

            //Hiển thị và truyền biến ra ngoài tầng View
            return view('carrer.index',compact('job_lists', 'job_carrer', 'high_job_list', 'expired_jobs', 'other_carrers', 'hot_job'));
        }else{
            return view('errors.404');
        }
    }

    //Lấy ra danh sách ngành nghề
    public function category()
    {
        $job_carrers = JobCarrer::where('is_active', 1)->orderBy('ranking', 'asc')->get();
        return view('carrer.category', compact('job_carrers'));
    }
}
