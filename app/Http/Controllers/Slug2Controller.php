<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\IndustrialZone;
use App\Models\Company;

class Slug2Controller extends Controller
{
    //Kiểm tra Slug2
    public function index($slug1, $slug2)
    {
        $province = Province::where('slug', $slug1)->where('is_active', 1)->first();
        $industrial_zone = IndustrialZone::where('slug', $slug2)->where('is_active', 1)->first();
        $company = Company::where('slug', $slug2)->where('is_active', 1)->first();

        if($province){
            if ($industrial_zone) { //Hiển thị trang việc làm khu công nghiệp
                $industrialZoneController = new IndustrialZoneController;
                return $industrialZoneController->index($slug1, $slug2);
            }elseif ($company){ //Hiển thị việc làm trang công ty
                $companyController = new CompanyController;
                return $companyController->index($slug1, $slug2);
            }else{
                return view('errors.404');
            }
        }else{
            return view('errors.404');
        }
    }
}
