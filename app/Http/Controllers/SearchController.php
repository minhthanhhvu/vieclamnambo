<?php

namespace App\Http\Controllers;

use Debugbar;
use Illuminate\Http\Request;
use App\Models\JobCarrer;
use App\Models\JobList;
use App\Models\Province;
use App\Models\Company;
use App\Models\IndustrialZone;
use App\Models\JobWage;
use DB;

class SearchController extends Controller
{
    //Hiển thị trang kết quả tìm kiếm
    public function index(Request $request)
    {
        $request->flash();

        //Lấy ra dữ liệu đưa vào ListBox tìm kiếm
        $listbox_carrers = JobCarrer::select('name', 'id')->where('is_active', 1)->orderBy('ranking', 'asc')->get();
        $listbox_provinces = Province::with('industrialZones')->select('name', 'id')->where('is_active', 1)->orderBy('ranking', 'asc')->get();

        //Lấy dữ liệu cần tìm kiếm qua phương thức GET
        $key = $request->key;
        $career_id = $request->career_id;
        $province_id = $request->province_id;
        $industrial_zone_id = $request->industrial_zone_id;

        //Tìm kiếm theo ngành nghề
        if(!empty($career_id)){
            $carrer = JobCarrer::active()->find($career_id);
            $job_lists = JobCarrer::with('jobLists')->find($career_id)->jobLists();
        }else{
            $carrer = null;
            $job_lists = new JobList;
        }

        //Sử dụng Eager Load để tối ưu câu lệnh truy vấn
        $job_lists = $job_lists->with('province', 'company', 'industrialZone', 'jobCarrers', 'jobWage');

        //Tìm kiếm theo tỉnh thành
        if(!empty($province_id)){
            $province = Province::active()->find($province_id);
            $job_lists = $job_lists->where('province_id', $province_id);
        }else{
            $province = null;
        }

        //Tìm kiếm theo Khu công nghiệp
        if (!empty($industrial_zone_id)){
            $industrial_zone = IndustrialZone::active()->find($industrial_zone_id);
            $job_lists->where('industrial_id', $industrial_zone_id);
        }else{
            $industrial_zone = null;
        }

        //Tìm kiếm theo Từ khóa
        if (!empty($key)) {
            $job_lists->where('job_title', 'like', '%'.$key.'%');
        }

        //Lấy ra kết quả tìm kiếm và phân trang
        $job_lists = $job_lists->active()->latest()->paginate(10);

        //Lấy việc làm nổi bật
        $hot_job = JobList::orderBy('updated_at', 'desc')->where('job_ended_at', '>', date('Y-m-d H:i:s'))->first();

        //Việc làm tuyển gấp
        $recuit_job = JobList::orderBy('job_ended_at', 'desc')->where('job_ended_at', '>', date('Y-m-d H:i:s'))->first();

        return view('search', compact('job_lists', 'listbox_carrers', 'listbox_provinces', 'listbox_industrial_zones', 'hot_job', 'recuit_job', 'carrer', 'province', 'industrial_zone', 'key'));
    }

    //Load ajax lấy ra danh sách khu công nghiệp có trong tỉnh thành đó
    public function loadIndustrialZone($province_id)
    {
        $province = Province::find($province_id);
        $industrial_zones = $province->industrialZones()->get();

        foreach ($industrial_zones as $industrial_zone){
            echo '<option value="'.$industrial_zone->id.'">'.$industrial_zone->name.'</option>';
        }
    }
}
