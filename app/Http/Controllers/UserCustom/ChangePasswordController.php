<?php

namespace App\Http\Controllers\UserCustom;

use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use Auth;
use App\Models\UserCustom;

class ChangePasswordController extends Controller
{
    public function edit()
    {
        return view('userCustom.changePassword');
    }

    public function update(ChangePasswordRequest $request)
    {
        //Kiểm tra đổi mật khẩu
        if(empty(Auth::user()->password) || Hash::check($request->old_password, Auth::user()->password)){
            $password = bcrypt($request->password);
            $user_custom = UserCustom::find(Auth::user()->id);
            $user_custom->update([
                'password' => $password,
            ]);
            session()->flash('flash_message', 'Đổi mật khẩu thành công');
        }else{
            $password = Auth::user()->password ;
            session()->flash('flash_message', 'Mật khẩu cũ sai');
        }

        return redirect('/doi-mat-khau');
    }
}
