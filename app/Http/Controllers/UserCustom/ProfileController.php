<?php

namespace App\Http\Controllers\UserCustom;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserCustom;
use App\Http\Requests\ProfileRequest;
use Auth;
use App\Models\Province;

class ProfileController extends Controller
{
    public function edit()
    {
        $user_custom = UserCustom::find(Auth::user()->id);
        $provinces = Province::where('is_active', 1)->orderBy('ranking', 'asc')->pluck('name','id');
        return view('userCustom.profile', compact('user_custom', 'provinces'));
    }

    public function update(ProfileRequest $request)
    {
        $user_custom = UserCustom::find(Auth::user()->id);
        $user_custom->update([
            'avatar' => $request->avatar,
            'fullname' => $request->fullname,
            'gender' => $request->gender,
            'province' => $request->province,
            'address' => $request->address,
            'birthday' => $request->birthday,
            'phone' => $request->phone,
            'email' => $request->email,
        ]);

        session()->flash('flash_message', 'Bạn đã cập nhật thông tin cá nhân thành công');
 
        return redirect('/thong-tin-ca-nhan');
    }
}
