<?php

namespace App\Http\Controllers\UserCustom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\CustomSaveJob;
use App\Models\JobList;

class SaveJobController extends Controller
{
    public function index()
    {
        //Lấy ra danh sách việc làm đã lưu và phân trang
        $custom_save_jobs = CustomSaveJob::with('jobList', 'jobList.province', 'jobList.jobWage', 'jobList.industrialZone', 'jobList.company')->where('is_active', 1)->where('custom_id', Auth::user()->id)->orderBy('updated_at', 'desc')->paginate(10);

        //Lấy việc làm nổi bật
        $hot_job = JobList::with('jobWage', 'jobExperience', 'industrialZone')->where('is_active', 1)->where('job_ended_at', '>', date('Y-m-d H:i:s'))->orderBy('updated_at', 'desc')->first();

        //Việc làm tuyển gấp
        $recuit_job = JobList::with('jobWage', 'jobExperience', 'industrialZone')->where('is_active', 1)->where('job_ended_at', '>', date('Y-m-d H:i:s'))->orderBy('job_ended_at', 'desc')->first();

        return view('userCustom.saveJob', compact('custom_save_jobs', 'hot_job', 'recuit_job'));
    }

    public function store($id)
    {
        if(Auth::user()) {
            $job = JobList::active()->find($id)->first();
            if ($job) {
                $custom_save_job = CustomSaveJob::where('job_id', $id)->where('custom_id', Auth::user()->id)->first();
                if ($custom_save_job) {
                    if ($custom_save_job->is_active == 0) {
                        $custom_save_job->is_active = 1;
                        $custom_save_job->save();
                    }
                }else {
                    $custom_save_job = new CustomSaveJob;
                    $custom_save_job->is_active = 1;
                    $custom_save_job->custom_id = Auth::user()->id;
                    $custom_save_job->job_id = $id;
                    $custom_save_job->save();
                }
                $result = 'valid';
            }else{
                $result = 'not-exists';
            }
        }else{
            $result = 'need-login';
        }
        echo $result;
    }

    public function destroy(Request $request)
    {
        $custom_save_job_ids = $request->custom_save_job_ids;
        if(count($custom_save_job_ids) > 0) {
            foreach ($custom_save_job_ids as $custom_save_job_id) {
                $custom_save_job = CustomSaveJob::find($custom_save_job_id);
                $custom_save_job->is_active = 0;
                $custom_save_job->save();
            }
            session()->flash('flash_message', 'Xóa thành công');
            return redirect('/viec-lam-da-luu');
        }else{
            session()->flash('flash_message', 'Bạn chưa chọn công việc cần xóa');
            return redirect('/viec-lam-da-luu');
        }
    }
}
