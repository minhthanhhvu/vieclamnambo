<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Page;

class Slug1Controller extends Controller
{
    //Kiểm tra Slug2
    public function index($slug1)
    {
        $province = Province::where('slug', $slug1)->where('is_active', 1)->first();
        $page = Page::where('slug', $slug1)->first();
        
        if($province) { //Nếu là trang tỉnh
            $provinceController = new ProvinceController;
            return $provinceController->index($slug1);
        }elseif($page){ //Nếu là page
            $pageController = new PageController;
            return $pageController->show($slug1);
        }else{ //Nếu sai URL
            return view('errors.404');
        }
    }
}
