<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Province;
use App\Models\JobList;

class CompanyController extends Controller
{
    public function index($slug1, $slug2)
    {
        $province = Province::with('jobLists')->active()->where('slug', $slug1)->first();
        $company = Company::with('jobLists')->active()->where('slug', $slug2)->first();

        if ($province && $company){
            //Lấy ra danh sách việc làm theo khu công nghiệp (có phân trang)
            $job_lists = $company->jobLists()->active()->latest()->paginate(5);

            //Lấy ra 5 việc làm trong khu công nghiệp
            $industrialzone_jobs = $company->jobLists()->active()->orderByEndedAt()->take(5)->get();

            return view('company',compact('job_lists', 'province', 'company', 'industrialzone_jobs'));
        }else{
            return view('errors.404');
        }
    }
}
