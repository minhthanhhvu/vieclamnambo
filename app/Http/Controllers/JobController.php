<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JobCarrer;
use App\Models\JobList;
use App\Models\Province;
use App\Models\Company;
use App\Models\IndustrialZone;
use Debugbar;

class JobController extends Controller
{
    //Hiển thị chi tiết việc làm
    public function index($slug1, $slug2, $slug3)
    {
        $province = Province::where('slug', $slug1)->where('is_active', 1)->first();
        $industrial_zone = IndustrialZone::where('slug', $slug2)->where('is_active', 1)->first();
        $job = JobList::where('slug', $slug3)->where('is_active', 1)->first();

        //Kiểm tra URL chi tiết việc làm
        if ($province && $industrial_zone && $job){
            //Việc làm liên quan
            $job_relates = JobList::active()->notExpired()->where('industrial_id', $industrial_zone->id)->latest()->take(5)->get();

            //Việc làm tương tự
            $job_sames = JobList::active()->notExpired()->where('province_id', $province->id)->where('industrial_id', '<>', $industrial_zone->id)->latest()->take(5)->get();

            return view('job', compact('job', 'job_relates', 'job_sames'));
        }else{
            return view('errors.404');
        }
    }
}
