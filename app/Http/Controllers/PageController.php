<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;

class PageController extends Controller
{
    public function show($slug)
    {
        $page = Page::where('is_active', 1)->where('slug', $slug)->first();
        if ($page) {
            return view('page', compact('page'));
        }else{
            return view('errors.404');
        }
    }
}
