<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\IndustrialZone;
use App\Models\JobList;
use App\Models\Company;

class IndustrialZoneController extends Controller
{
    //Việc làm theo Khu công nghiệp
    public function index($slug1, $slug2)
    {
        $province = Province::with('jobLists')->active()->where('slug', $slug1)->first();
        $industrial_zone = IndustrialZone::with('jobLists')->active()->where('slug', $slug2)->first();

        if ($province && $industrial_zone){
            //Lấy ra danh sách việc làm theo khu công nghiệp (có phân trang)
            $job_lists = $industrial_zone->jobLists()->active()->latest()->paginate(10);

            //Lấy ra 5 việc làm gấp (sắp hết hạn)
            $expired_job_lists = $industrial_zone->jobLists()->active()->expiredSoon()->orderByEndedAt()->take(5)->get();

            //Lấy ra 5 việc làm hấp dẫn (lương cao nhất)
            $high_job_lists = $industrial_zone->jobLists()->active()->latest()->take(5)->get();

            //Hiển thị công ty trong KCN
            $company_industrialzones = $industrial_zone->companies()->active()->inRandomOrder()->take(5)->get();

            return view('industrialZone',compact('job_lists', 'province', 'industrial_zone', 'high_job_lists', 'expired_job_lists', 'company_industrialzones'));
        }else{
            return view('errors.404');
        }
    }


    public function getImagePathAttribute($value)
    {
        if ($value){
            $result = explode(',', $value);
        }else{
            $result = null;
        }
        return $result;
    }
}
