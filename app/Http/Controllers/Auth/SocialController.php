<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use Auth;
use App\Models\Social;
use App\Models\UserCustom;

class SocialController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        $social = Social::where('provider_user_id', $user->id)->where('provider', 'facebook')->first();
        if ($social){
            $u = UserCustom::where('email', $user->email)->first();
            Auth::login($u);
            return redirect('/');
        }else{
            $temp = new Social;
            $temp->provider_user_id = $user->id;
            $temp->provider = 'facebook';

            $u = UserCustom::where('email', $user->email)->first();
            if (!$u){
                $u = UserCustom::create([
                    'fullname' => $user->name,
                    'email' => $user->email,
                ]);
            }
            $temp->user_custom_id = $u->id;

            $temp->save();

            Auth::login($u);
            return redirect('/');
        }
    }
}
