<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface JobCarrerRepository
 * @package namespace App\Repositories\Contracts;
 */
interface JobCarrerRepository extends RepositoryInterface
{
    //
}
