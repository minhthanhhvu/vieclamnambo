<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmployerRepository
 * @package namespace App\Repositories\Contracts;
 */
interface EmployerRepository extends RepositoryInterface
{
    //
}
