<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface JobListRepository
 * @package namespace App\Repositories\Contracts;
 */
interface JobListRepository extends RepositoryInterface
{
    //
}
