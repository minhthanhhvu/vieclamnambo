<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface JobExperienceRepository
 * @package namespace App\Repositories\Contracts;
 */
interface JobExperienceRepository extends RepositoryInterface
{
    //
}
