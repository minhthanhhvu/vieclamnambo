<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface JobWageRepository
 * @package namespace App\Repositories\Contracts;
 */
interface JobWageRepository extends RepositoryInterface
{
    //
}
