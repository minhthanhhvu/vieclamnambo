<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface IndustrialZoneRepository
 * @package namespace App\Repositories\Contracts;
 */
interface IndustrialZoneRepository extends RepositoryInterface
{
    //
}
