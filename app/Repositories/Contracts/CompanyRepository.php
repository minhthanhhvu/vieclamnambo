<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CompanyRepository
 * @package namespace App\Repositories\Contracts;
 */
interface CompanyRepository extends RepositoryInterface
{
    //
}
