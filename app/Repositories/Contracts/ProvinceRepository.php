<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProvinceRepository
 * @package namespace App\Repositories\Contracts;
 */
interface ProvinceRepository extends RepositoryInterface
{
    //
}
