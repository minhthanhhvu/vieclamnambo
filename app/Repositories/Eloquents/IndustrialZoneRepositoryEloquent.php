<?php

namespace App\Repositories\Eloquents;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\IndustrialZoneRepository;
use App\Models\IndustrialZone;
use App\Validators\IndustrialZoneValidator;

/**
 * Class IndustrialZoneRepositoryEloquent
 * @package namespace App\Repositories\Eloquents;
 */
class IndustrialZoneRepositoryEloquent extends BaseRepository implements IndustrialZoneRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return IndustrialZone::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
