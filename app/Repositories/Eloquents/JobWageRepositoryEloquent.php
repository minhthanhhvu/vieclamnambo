<?php

namespace App\Repositories\Eloquents;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\JobWageRepository;
use App\Models\JobWage;
use App\Validators\JobWageValidator;

/**
 * Class JobWageRepositoryEloquent
 * @package namespace App\Repositories\Eloquents;
 */
class JobWageRepositoryEloquent extends BaseRepository implements JobWageRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return JobWage::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
