<?php

namespace App\Repositories\Eloquents;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\EmployerRepository;
use App\Models\Employer;
use App\Validators\EmployerValidator;

/**
 * Class EmployerRepositoryEloquent
 * @package namespace App\Repositories\Eloquents;
 */
class EmployerRepositoryEloquent extends BaseRepository implements EmployerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Employer::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
