<?php

namespace App\Repositories\Eloquents;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\JobCarrerRepository;
use App\Models\JobCarrer;
use App\Validators\JobCarrerValidator;

/**
 * Class JobCarrerRepositoryEloquent
 * @package namespace App\Repositories\Eloquents;
 */
class JobCarrerRepositoryEloquent extends BaseRepository implements JobCarrerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return JobCarrer::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
