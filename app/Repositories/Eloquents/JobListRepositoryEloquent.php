<?php

namespace App\Repositories\Eloquents;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\JobListRepository;
use App\Models\JobList;
use App\Validators\JobListValidator;

/**
 * Class JobListRepositoryEloquent
 * @package namespace App\Repositories\Eloquents;
 */
class JobListRepositoryEloquent extends BaseRepository implements JobListRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return JobList::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
