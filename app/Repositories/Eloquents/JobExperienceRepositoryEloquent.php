<?php

namespace App\Repositories\Eloquents;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\JobExperienceRepository;
use App\Models\JobExperience;
use App\Validators\JobExperienceValidator;

/**
 * Class JobExperienceRepositoryEloquent
 * @package namespace App\Repositories\Eloquents;
 */
class JobExperienceRepositoryEloquent extends BaseRepository implements JobExperienceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return JobExperience::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
