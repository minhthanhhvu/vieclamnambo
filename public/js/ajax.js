//Load Khu công nghiệp khi chọn Tỉnh thành
$('#province_id').change(function(){
    var province_id = $(this).val();
    if (province_id == ''){
        $('#industrial_zone_id').html('<option value="">Chọn khu công nghiệp</option>');
        $('#industrial_zone_id').prop('disabled', true);
    }else {
        var url = '/tim-kiem/ajax/' + province_id;
        $.get(url, function (data) {
            $('#industrial_zone_id').html('<option value="">Chọn khu công nghiệp</option>');
            $('#industrial_zone_id').append(data);
            $('#industrial_zone_id').prop('disabled', false);
        })
    }
});

//Lưu công việc
$('.save-job').click(function(){
    var job_id = $(this).attr('data-id');
    var url = '/luu-viec-lam/'+ job_id;
    console.log(url);
    $.get(url, function(data){
        if(data == 'valid'){
            alert('Lưu công việc thành công');
        }else if(data == 'need-login'){
            alert('Bạn cần đăng nhập để lưu công việc');
        }else if(data == 'not-exists'){
            alert('Việc làm không tồn tại hoặc đã bị xóa bỏ');
        }else{
            alert('Lưu công việc không thành công');
        }
    });
});