$(document).ready(function(){
    $('.result').mCustomScrollbar({
        scrollButtons:{enable:true},
        theme:"3d"
    });
    $('select.form-control').select2();
    if($('#fileinput')){
        $('#fileinput').fileinput({ showUpload: false,
            maxFileCount: 10,
            mainClass: "input-group-lg",initialCaption: "Tải ảnh lên"});
    }
    if($('.inputShowPwd')){
        var _input = new inputShowPwd('inputShowPwd');
    }
    if($('#popup_success')){
        $('#popup_success').modal();
    };
    $('.slides').nivoSlider({
        effect: 'random',
        slices: 15,
        boxCols: 8,
        boxRows: 4,
        animSpeed: 500,
        pauseTime: 3000,
        startSlide: 9,
        directionNav: false,
        controlNav: false,
        controlNavThumbs: true,
        pauseOnHover: true,
        manualAdvance: false,
        prevText: 'Prev',
        nextText: 'Next',
        randomStart: false,
        beforeChange: function(){},
        afterChange: function(){},
        slideshowEnd: function(){},
        lastSlide: function(){},
        afterLoad: function(){}
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        maxViewMode: 0,
        todayBtn: true,
        orientation: "top auto",
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true
    });
    if($(".back-to-top").length > 0){
        $(window).scroll(function () {
            var e = $(window).scrollTop();
            if (e > 300) {
                $(".back-to-top").show()
            } else {
                $(".back-to-top").hide()
            }
        });
    }
    $('#fileinput').fileinput({
        previewFileType: "image",
        browseClass: "btn btn-success",
        browseLabel: "Pick Image",
        browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
        removeClass: "btn btn-danger",
        removeLabel: "Delete",
        removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",
        uploadClass: "btn btn-info",
        uploadLabel: "Upload",
        uploadIcon: "<i class=\"glyphicon glyphicon-upload\"></i> "
    });
});
window.addEventListener('resize', function() {
    if(window.innerWidth <767){
        $('#menu').mmenu({
            "extensions": [
                "shadow-page",
                "shadow-panels",
                "effect-menu-fade",
                "effect-listitems-slide"
            ]
            ,
            navbar 		: {
                title : " "
            },
            'navbars' : [
                {
                    "position": 'top',
                    "content": $('#menu-header').html()
                },
                {
                    "position" : "top",
                    "content" : $('#menu-footer').html()
                },
                {
                    "position": "bottom",
                    "content": [
                        "<a class='fa fa-envelope' href='#/'></a>",
                        "<a class='fa fa-twitter' href='#/'></a>",
                        "<a class='fa fa-facebook' href='#/'></a>"
                    ]
                }
            ]
        });
    }
});
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('.back-to-top').addClass('show');
    } else {
        $('.back-to-top').removeClass('show');
    }
});
$(".back-to-top").click(function (e) {
    e.preventDefault();
    $('body,html').animate({
        scrollTop: 0
    },800)
})
