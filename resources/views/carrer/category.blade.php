@extends($layout)

@section('title', 'Ngành nghề - Việc Làm Nam Bộ | Cổng thông tin việc làm hàng đầu khu vực phía Nam')
@section('keywords', '')
@section('description', '')

@section('content')
    <div id="category">
        <div class="container">
            <div class="category">
                <div class="box-head">
                    <h3>Tất cả ngành nghề</h3>
                </div>
                <div class="box-body">
                    <div class="form-category">
                        <div class="row">
                            @foreach ($job_carrers as $job_carrer)
                            <a class="category-single col-md-3 col-xs-6" href="/nganh-nghe/{{ $job_carrer->slug }}">
                                <img class="salesLogoImage" src="{{ $job_carrer->icon ? $job_carrer->icon : '/image/no-image.png' }}">
                                <p class="text-center category-name">{{ $job_carrer->name }}</p>
                                {{--
                                <p class="text-center category-job">
                                    ( <span style="color:red">12</span> việc làm mới )
                                </p>
                                --}}
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection