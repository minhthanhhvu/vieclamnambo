@extends($layout)

@section('title', $job_carrer->meta_title)
@section('keywords', '')
@section('description', '')

@section('content')
    <div id="main">
        <div class="container">
            <h3 class="text-uppercase">Việc làm {{ $job_carrer->name }}</h3>
            <div class="row">
                <div id="content" class="col-md-8">
                    @if($job_lists->count() > 0)
                        <div class="form-list-job">
                        <div class="box-head">
                            <h3>VIỆC LÀM MỚI NHẤT</h3>
                        </div>
                        <div class="box-body">
                                <div class="list-jobs">
                                    @foreach ($job_lists as $job_list)
                                        <div class="list-item">
                                            <div class="row">
                                                <a href="/{{ $job_list->province->slug }}/{{ $job_list->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{!! $job_list->company->logo ? $job_list->company->logo : '/image/no-image.png' !!}" alt="{{ $job_list->company->name }}"></a>
                                                <div class="list-item-content col-md-10 col-xs-9">
                                                    <div  class="company-job"><h2><a href="/{{ $job_list->province->slug }}/{{ $job_list->industrialZone->slug }}/{{ $job_list->slug }}.html">{{ $job_list->job_title }}</a></h2><!-- <span class="label label-danger">HOT</span> --></div>
                                                    <div class="row">
                                                        <div class="col-md-4 mp">
                                                            <i class="fa fa-map-marker" aria-hidden="true"></i><a href="/{{ $job_list->province->slug }}">{{ $job_list->province->name }}</a>
                                                        </div>
                                                        <div class="col-md-4 mp">
                                                            <span><i class="fa fa-usd" aria-hidden="true"></i> {{ $job_list->jobWage ? $job_list->jobWage->name : number_format($job_list->job_wage) }}</span>
                                                        </div>
                                                        <div class="col-md-4 mp">
                                                            <span><i class="fa fa-stop-circle-o" aria-hidden="true"></i> {{ $job_list->job_ended_at }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="page">
                                    {!! $job_lists->render() !!}
                                </div>
                        </div>
                    </div>
                    @else
                        <p>Không có việc làm nào trong ngành nghề này</p>
                    @endif

                    @if ($expired_jobs->count() > 0)
                        <div class="form-list-job">
                            <div class="box-head">
                                <h3>Việc làm tuyển gấp</h3>
                            </div>
                            <div class="box-body">
                                <div class="list-jobs">
                                    @foreach ($expired_jobs as $expired_job)
                                        <div class="list-item">
                                            <div class="row">
                                                <a href="/{{ $expired_job->province->slug }}/{{ $expired_job->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{!! $expired_job->company->logo ? $expired_job->company->logo : '/image/no-image.png' !!}" alt="{{ $expired_job->company->name }}"></a>
                                                <div class="list-item-content col-md-10 col-xs-9">
                                                    <div class="company-job"><h2><a href="/{{ $expired_job->province->slug }}/{{ $expired_job->industrialZone->slug }}/{{ $expired_job->slug }}.html">{{ $expired_job->job_title }}</a></h2><span class="label label-danger">GẤP</span></div>
                                                    <div class="row">
                                                        <div class="col-md-4 mp">
                                                            <i class="fa fa-map-marker" aria-hidden="true"></i><a href="/{{ $expired_job->province->slug }}">{{ $expired_job->province->name }}</a>
                                                        </div>
                                                        <div class="col-md-4 mp">
                                                            <span><i class="fa fa-usd" aria-hidden="true"></i> {{ $expired_job->jobWage ? $expired_job->jobWage->name : number_format($expired_job->job_wage) }}</span>
                                                        </div>
                                                        <div class="col-md-4 mp">
                                                            <i class="fa fa-stop-circle-o" aria-hidden="true"></i>
                                                            @if ($expired_job->subJobEndedAt())
                                                                Còn <span style="color:red">{{ $expired_job->subJobEndedAt() }}</span> ngày để nộp HS
                                                            @else
                                                                <span style="color:red">Hết hạn</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <div id="sidebar" class="col-md-4">
                    @if ($hot_job)
                        <div class="job-one">
                            <div class="box-head">
                                <h3>Việc làm nổi bật</h3>
                            </div>
                            <div class="box-body">
                                <div class="company-job"><h4><a href="/{{ $hot_job->province->slug }}/{{ $hot_job->industrialZone->slug }}/{{ $hot_job->slug }}.html">{{ $hot_job->job_title }}</a>{{ $hot_job->checkHot() }}</h4></div>
                                <p><i class="fa fa-address-book" aria-hidden="true"></i><a href="/{{ $hot_job->province->slug }}/{{ $hot_job->company->slug }}">{{ $hot_job->company->name }}</a></p>
                                <p class="block-50"><i class="fa fa-map-marker" aria-hidden="true"></i><a href="/{{ $hot_job->province->slug }}">{{ $hot_job->province->name }}</a></p>
                                <p class="block-50"><i class="fa fa-usd" aria-hidden="true"></i>{{ $hot_job->jobWage ? $hot_job->jobWage->name : number_format($hot_job->job_wage) }}</p>
                                <p class="block-50"><i class="fa fa-user-secret" aria-hidden="true"></i>{{ $hot_job->jobExperience->name }}</p>
                                <p class="block-50"><i class="fa fa-stop-circle-o" aria-hidden="true"></i>{{ $hot_job->job_ended_at }}</p>
                                <p>{!! str_limit($hot_job->description, 200) !!}</p>
                                <a href="/{{ $hot_job->province->slug }}/{{ $hot_job->industrialZone->slug }}/{{ $hot_job->slug }}.html" class="more">Xem chi tiết</a>
                            </div>
                        </div>
                    @endif

                    <div class="form-list-right">
                        <div class="box-head">
                            <h3>Các ngành nghề khác</h3>
                        </div>
                        <div class="box-body">
                            <div class="list-job">
                                @foreach ($other_carrers as $other_carrer)
                                <div class="list-item-job row">
                                    <div class="img-logo-job col-md-2 col-xs-3">
                                        <a href="/nganh-nghe/{{ $other_carrer->slug }}"><img src="{{ $other_carrer->icon ? $other_carrer->icon : '/image/no-image.png' }}" alt=""></a>
                                    </div>
                                    <div class="list-item-content-job col-md-10 col-xs-9 mp">
                                        <a href="/nganh-nghe/{{ $other_carrer->slug }}" class="">{{ $other_carrer->name }} {{-- ( <span style="color:red;font-size: 14px;">102</span> ) --}}</a>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection