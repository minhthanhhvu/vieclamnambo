@extends($layout)

@section('title', $industrial_zone->meta_title)
@section('description', $industrial_zone->meta_desciption)

@section('head')
    <style type="text/css">
        #map-canvas { height: 100% }
    </style>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfNk5eVWmQB9e6ApnWzICLNIY5lUXpOBw"></script>
    <script type="text/javascript">

        // Function khởi tạo google map
        function initialize()
        {
            // Config google map
            var mapOptions = {
                // Tọa độ muốn hiển thị ban đầu (tung độ,vỹ độ)
                center: new google.maps.LatLng(10.771971, 106.697845),
                // Mức độ zoom
                zoom: 8
            };

            // Hiển thị map lên bản đồ (div#map-canvas)
            var map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
        }

        // Gán hàm initialize vào trong sự kiện load dom google map
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection

@section('content')
    <div id="main-top">
        <div class="container">
            <h3>Việc làm {{ $industrial_zone->name }}</h3>
            <div class="main-top">
                <div class="col-md-4 main-top-img">
                    <img src="{!! $industrial_zone->image_path ? $industrial_zone->image_path : '/image/no-image.png' !!}" title="{{ $industrial_zone->name }}" alt="{{ $industrial_zone->name }}" width="498">
                </div>
                <div class="col-md-5 desc">
                    {!! str_limit($industrial_zone->introduction, 400) !!}
                </div>

                <div class="col-md-3 map">
                    <div class="box">
                        <div id="map-canvas"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="main">
        <div class="container">
            <div class="row">
                <div id="content" class="col-md-8">
                    @if ($job_lists->count() > 0)
                    <div class="form-list-job">
                        <div class="box-head">
                            <h3>VIỆC LÀM MỚI NHẤT</h3>
                        </div>
                        <div class="box-body">
                            <div class="list-jobs">
                                @foreach ($job_lists as $job_list)
                                    <div class="list-item">
                                        <div class="row">
                                            <a href="/{{ $job_list->province->slug }}/{{ $job_list->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{!! $job_list->company->logo ? $job_list->company->logo : '/image/no-image.png' !!}" alt="{{ $job_list->company->name }}"></a>
                                            <div class="list-item-content col-md-10 col-xs-9">
                                                <div  class="company-job"><h2><a href="/{{ $job_list->province->slug }}/{{ $job_list->industrialZone->slug }}/{{ $job_list->slug }}.html">{{ $job_list->job_title }}</a></h2><!-- <span class="label label-danger">HOT</span> --></div>
                                                <div class="row">
                                                    <div class="col-md-4 mp">
                                                        @if ($job_list->jobCarrers)
                                                            <i class="fa fa-suitcase" aria-hidden="true"></i>
                                                            @foreach ($job_list->jobCarrers as $jobCarrer)
                                                                <a href="/nganh-nghe/{{ $jobCarrer->slug }}">{{ $jobCarrer->name }}</a>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                    <div class="col-md-4 mp">
                                                        <span><i class="fa fa-usd" aria-hidden="true"></i> {{ $job_list->jobWage ? $job_list->jobWage->name : number_format($job_list->job_wage) }}</span>
                                                    </div>
                                                    <div class="col-md-4 mp">
                                                        <span><i class="fa fa-stop-circle-o" aria-hidden="true"></i>{{ $job_list->job_ended_at }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="page">
                                {!! $job_lists->render() !!}
                            </div>
                        </div>
                    </div>
                    @else
                        <p>Không có việc làm mới trong khu công nghiệp này</p>
                    @endif
                </div>

                <div id="sidebar" class="col-md-4">
                    <div class="form-list-right">
                        <div class="box-head">
                            <h3>Công ty trong khu công nghiệp</h3>
                        </div>
                        <div class="box-body">
                            @foreach ($company_industrialzones as $company_industrialzone)
                            <div class="list-item">
                                <div class="row">
                                    <a href="/{{ $company_industrialzone->province->slug }}/{{ $company_industrialzone->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{{ $company_industrialzone->logo ? $company_industrialzone->logo : '/image/no-image.png' }}" alt="{{ $company_industrialzone->name }}"></a>
                                    <div class=" col-md-10 col-xs-9 list-item-content">
                                        <div class="company-job"><h4><a href="/{{ $company_industrialzone->province->slug }}/{{ $company_industrialzone->slug }}">{{ $company_industrialzone->name }}</a></h4></div>
                                        <div class="company-num">
                                            Đang có <span style="color:red">{{ number_format($company_industrialzone->jobLists->count()) }}</span> việc
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="video">
                        <div class="box-head">
                            <h3>Video</h3>
                        </div>
                        <div class="box-body" style="margin: 0;padding: 0;">
                            <iframe width="100%" height="300" style="padding: 0; border: 0;margin: 0;" src="https://www.youtube.com/embed/haLuAFxOwWQ">
                            </iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection