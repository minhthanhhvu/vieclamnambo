
@extends($layout)

@section('title', $province->meta_title)
@section('keywords', '')
@section('description', $province->meta_description)

@section('content')
<div id="main-top">
    <div class="container">
        <h3>Việc làm {{ $province->name }}</h3>
        <div class="main-top">
            <div id="slides" class="col-md-5 main-top-img">
                <div class="slides">
                    @if($province->images)
                        @foreach ($province->images as $image)
                            <a href="/{{ $province->slug }}"><img src="{{ $image }}" title="{{ $province->name }}" alt="{{ $province->name }}" width="498" height="315"></a>
                        @endforeach
                    @else
                        <a href="/{{ $province->slug }}"><img src="/image/no-image.png" title="{{ $province->name }}" alt="{{ $province->name }}" width="498" height="315"></a>
                    @endif
                </div>
            </div>
            <div class="col-md-7 desc">
                {!! $province->introduction !!}
            </div>
        </div>
    </div>
</div>

<div id="main">
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-8">
                <div class="form-list-job">
                    <div class="box-head">
                        <h3>VIỆC LÀM MỚI NHẤT</h3>
                    </div>
                    <div class="box-body">
                        <div class="list-jobs">
                            @foreach ($job_lists as $job_list)
                            <div class="list-item">
                                <div class="row">
                                    <a href="/{{ $job_list->province->slug }}/{{ $job_list->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{{ $job_list->company->logo ? $job_list->company->logo : '/image/no-image.png' }}" alt="{{ $job_list->company->name }}"></a>
                                    <div class="list-item-content col-md-10 col-xs-9">
                                        <div  class="company-job"><h2><a href="/{{ $job_list->province->slug }}/{{ $job_list->industrialZone->slug }}/{{ $job_list->slug }}.html">{{ $job_list->job_title }}</a></h2>{{-- <span class="label label-danger">HOT</span> --}}</div>
                                        <div class="row">
                                            <div class="col-md-8 mp">
                                                <i class="fa fa-address-book" aria-hidden="true"></i><a href="/{{ $job_list->province->slug }}/{{ $job_list->company->slug }}">{{ $job_list->company->name }}</a>
                                            </div>
                                            <div class="col-md-4 mp">
                                                <span><i class="fa fa-usd" aria-hidden="true"></i> {{ $job_list->jobWage ? $job_list->jobWage->name : number_format($job_list->job_wage) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="page">
                            {!! $job_lists->render() !!}
                        </div>

                    </div>
                </div>

                @if ($expired_job_lists->count() > 0)
                <div class="form-list-job">
                    <div class="box-head">
                        <h3>Việc làm tuyển gấp</h3>
                    </div>
                    <div class="box-body">
                        <div class="list-jobs">
                            @foreach ($expired_job_lists as $expired_job_list)
                            <div class="list-item">
                                <div class="row">
                                    <a href="/{{ $expired_job_list->province->slug }}/{{ $expired_job_list->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{!! $expired_job_list->company->logo ? $expired_job_list->company->logo : '/image/no-image.png' !!}" alt="{{ $expired_job_list->company->name }}"></a>
                                    <div class="list-item-content col-md-10 col-xs-9">
                                        <div class="company-job"><h2><a href="/{{ $expired_job_list->province->slug }}/{{ $expired_job_list->industrialZone->slug }}/{{ $expired_job_list->slug }}.html">{{ $expired_job_list->job_title }}</a></h2><span class="label label-danger">GẤP</span></div>
                                        <div class="row">
                                            <div class="col-md-8 mp">
                                                <i class="fa fa-address-book" aria-hidden="true"></i><a href="/{{ $expired_job_list->province->slug }}/{{ $expired_job_list->company->slug }}">{{ $expired_job_list->company->name }}</a>
                                            </div>
                                            <div class="col-md-4 mp">
                                                <i class="fa fa-stop-circle-o" aria-hidden="true"></i>
                                                @if ($expired_job_list->subJobEndedAt())
                                                Còn <span style="color:red">{{ $expired_job_list->subJobEndedAt() }}</span> ngày để nộp HS
                                                @else
                                                    <span style="color:red">Hết hạn</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
            </div>

            <div id="sidebar" class="col-md-4">
                @if($high_job_lists->count() > 0)
                <div class="form-list-right">
                    <div class="box-head">
                        <h3>Việc làm hấp dẫn</h3>
                    </div>
                    <div class="box-body">
                        @foreach ($high_job_lists as $high_job_list)
                        <div class="list-item">
                            <div class="row">
                                <a href="/{{ $high_job_list->province->slug }}/{{ $high_job_list->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{!! $high_job_list->company->logo ? $high_job_list->company->logo : '/image/no-image.png' !!}" alt="{{ $high_job_list->company->name }}"></a>
                                <div class=" col-md-10 col-xs-9 list-item-content">
                                    <div class="company-job"><h4><a href="/{{ $high_job_list->province->slug }}/{{ $high_job_list->industrialZone->slug }}/{{ $high_job_list->slug }}.html">{{ $high_job_list->job_title }}</a><span class="label label-danger">HOT</span></h4></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <i class="fa fa-usd" aria-hidden="true"></i>  {{ $high_job_list->jobWage ? $high_job_list->jobWage->name : number_format($high_job_list->job_wage) }}
                                        </div>
                                        <div class="col-md-6">
                                            <i class="fa fa-stop-circle-o" aria-hidden="true"></i>{{ $high_job_list->job_ended_at }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif

                <div class="video">
                    <div class="box-head">
                        <h3>Video</h3>
                    </div>
                    <div class="box-body">
                        <iframe width="100%" height="300" src="https://www.youtube.com/embed/haLuAFxOwWQ">
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection