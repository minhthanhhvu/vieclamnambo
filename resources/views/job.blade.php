@extends($layout)

@section('title', $job->meta_title)
@section('description', $job->meta_description)

@section('content')
<div id="main">
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-8">
                <div id="post-cv">
                    <h3>{{ $job->industrialZone->name }}</h3>
                    <div class="job-top mp_12">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 mp">
                                    <div class="row">
                                        <div class="col-md-12 mp">
                                            <p class="mp_12">
                                            <h1>{{ $job->job_title }}</h1>
                                            <p class="mp_12" style="font-weight:bold;font-size:20px">
                                                <a href="/{{ $job->province->slug}}/{{ $job->company->slug }}">{{ $job->company->name }}</a>
                                            </p>
                                            </p>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-7 col-sm-6">
                                                @if ($job->jobCarrers->count() > 0)
                                                <p class="mp_12">
                                                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                                                    <strong>Ngành : </strong>
                                                        @foreach ($job->jobCarrers as $jobCarrer)
                                                            <a href="/nganh-nghe/{{ $jobCarrer->slug }}">{{ $jobCarrer->name }}</a>
                                                        @endforeach
                                                </p>
                                                @endif

                                                <p class="mp_12">
                                                    <i class="fa fa-transgender" aria-hidden="true"></i>
                                                    <strong>Giới tính : </strong>
                                                    @if($job->job_gender == 0)
                                                        Nữ
                                                    @elseif($job->job_gender == 1)
                                                        Nam
                                                    @else
                                                        Không yêu cầu
                                                    @endif
                                                </p>

                                                    @if ($job->province)
                                                <p class="mp_12">
                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    <strong>Địa chỉ : </strong>
                                                    {{ $job->province->name }}
                                                </p>
                                                    @endif

                                                @if ($job->job_number)
                                                <p class="mp_12">
                                                    <i class="fa fa-users" aria-hidden="true"></i>
                                                    <strong>Số lượng : </strong>
                                                    {{ number_format($job->job_number) }}
                                                </p>
                                                @endif
                                            </div>
                                            <div class="col-md-5 col-sm-6">
                                                @if ($job->jobWage)
                                                <p class="mp_12">
                                                    <i class="fa fa-usd" aria-hidden="true"></i>
                                                    <strong>Lương : </strong>
                                                    {{ $job->jobWage ? $job->jobWage->name : number_format($job->job_wage).' đ' }}
                                                </p>
                                                @endif

                                                @if ($job->jobLevel)
                                                <p class="mp_12">
                                                    <i class="fa fa-th-list" aria-hidden="true"></i>
                                                    <strong>Cấp bậc : </strong>
                                                    {{ $job->jobLevel->name }}
                                                </p>
                                                @endif

                                                <p class="mp_12">
                                                    <i class="fa fa-stop-circle-o" aria-hidden="true"></i>
                                                    <strong>Hạn nộp hồ sơ : </strong>
                                                    {{ $job->job_ended_at ? $job->job_ended_at : 'Hết hạn' }}
                                                </p>

                                                    @if($job->job_require_level)
                                                <p class="mp_12">
                                                    <i class="fa fa-tasks" aria-hidden="true"></i>
                                                    <strong>Trình độ : </strong>
                                                    {{ $job->job_require_level }}
                                                </p>
                                                        @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="job-content mp_12">
                        <div class="box-body">
                            <div class="job-description p_24">
                                @if ($job->description)
                                <div class="item-job row">
                                    <div class="col-md-3">
                                        <p class="item-job-title">Mô tả : </p>
                                    </div>
                                    <div class="col-md-9">
                                        <p class="item-job-content">
                                            {!! $job->description !!}
                                        </p>
                                    </div>
                                </div>
                                @endif

                                @if($job->job_require)
                                <div class="item-job row">
                                    <div class="col-md-3">
                                        <p class="item-job-title">Yêu cầu : </p>
                                    </div>
                                    <div class="col-md-9">
                                        <p class="item-job-content">
                                            {{ $job->job_require }}
                                        </p>
                                    </div>
                                </div>
                                @endif

                                @if ($job->job_benefit)
                                <div class="item-job row">
                                    <div class="col-md-3">
                                        <p class="item-job-title">Quyền lợi : </p>
                                    </div>
                                    <div class="col-md-9">
                                        @foreach ($job->job_benefit as $benefit)
                                            @if ($benefit)
                                                <p class="item-job-content">
                                                    {{ $benefit->name }}
                                                </p>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                                @endif

                                @if ($job->contact_description)
                                <div class="item-job row">
                                    <div class="col-md-3">
                                        <p class="item-job-title">Hồ sơ : </p>
                                    </div>
                                    <div class="col-md-9">
                                        <p class="item-job-content">
                                            {!! $job->contact_description !!}
                                        </p>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="job-footer mp_12">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="contact">Thông tin liên hệ</div>

                                    @if ($job->contact_name)
                                    <div class="item-contact row">
                                        <div class="col-md-3">
                                            <p class="item-contact-title">
                                                Liên hệ :
                                            </p>
                                        </div>
                                        <div class="col-md-9">
                                            <p class="item-contact-content">
                                                {{ $job->contact_name }}
                                            </p>
                                        </div>
                                    </div>
                                    @endif

                                    @if ($job->contact_addr)
                                    <div class="item-contact row">
                                        <div class="col-md-3">
                                            <p class="item-contact-title">
                                                Địa chỉ :
                                            </p>
                                        </div>
                                        <div class="col-md-9">
                                            <p class="item-contact-content">
                                                {{ $job->contact_addr }}
                                            </p>
                                        </div>
                                    </div>
                                    @endif

                                    @if ($job->contact_phone)
                                    <div class="item-contact row">
                                        <div class="col-md-3">
                                            <p class="item-contact-title">
                                                Điện thoại :
                                            </p>
                                        </div>
                                        <div class="col-md-9">
                                            <p class="item-contact-content">
                                                {{ $job->contact_phone }}
                                            </p>
                                        </div>
                                    </div>
                                    @endif

                                    @if ($job->contact_email)
                                    <div class="item-contact row">
                                        <div class="col-md-3">
                                            <p class="item-contact-title">
                                                Email :
                                            </p>
                                        </div>
                                        <div class="col-md-9">
                                            <p class="item-contact-content">
                                                {{ $job->contact_email }}
                                            </p>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="job-button mp_12">
                        <div class="box-body">
                            <div class="row">
                                {{-- <a href="#">Nộp đơn</a> --}}
                                @if(Auth::user())
                                    <a href="javascript:;" data-id="{{ $job->id }}" class="save-job">Lưu công việc</a>
                                @else
                                    <a href="/login">Lưu công việc</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="sidebar" class="col-md-4">
                @if ($job_sames)
                <div class="form-list-right">
                    <div class="box-head">
                        <h3>VIỆC LÀM LIÊN QUAN</h3>
                    </div>
                    <div class="box-body">
                        @foreach ($job_relates as $job_relate)
                        <div class="list-item">
                            <div class="row">
                                <a href="/{{ $job->province->slug }}/{{ $job->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{{ $job->company->logo ? $job->company->logo : '/image/no-image.png' }}" alt="{{ $job->company->name }}"></a>
                                <div class=" col-md-10 col-xs-9 list-item-content">
                                    <div class="company-job"><h4><a href="/{{ $job_relate->province->slug }}/{{ $job_relate->industrialZone->slug }}/{{ $job_relate->slug }}.html">{{ $job_relate->job_title }}</a></h4></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <i class="fa fa-usd" aria-hidden="true"></i>{{ $job_relate->jobWage ? $job_relate->jobWage->name : number_format($job_relate->job_wage) }}
                                        </div>
                                        <div class="col-md-6">
                                            <i class="fa fa-stop-circle-o" aria-hidden="true"></i>{{ $job_relate->job_ended_at }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif

                    @if ($job_sames)
                        <div class="form-list-right">
                            <div class="box-head">
                                <h3>VIỆC LÀM TƯƠNG TỰ</h3>
                            </div>
                            <div class="box-body">
                                @foreach ($job_sames as $job_same)
                                    <div class="list-item">
                                        <div class="row">
                                            <a href="/{{ $job->province->slug }}/{{ $job->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{{ $job->company->logo ? $job->company->logo : '/image/no-image.png' }}" alt="{{ $job->company->name }}"></a>
                                            <div class=" col-md-10 col-xs-9 list-item-content">
                                                <div class="company-job"><h4><a href="/{{ $job_same->province->slug }}/{{ $job_same->industrialZone->slug }}/{{ $job_same->slug }}.html">{{ $job_same->job_title }}</a></h4></div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <i class="fa fa-usd" aria-hidden="true"></i>{{ $job_same->jobWage ? $job_same->jobWage->name : number_format($job_same->job_wage) }}
                                                    </div>
                                                    <div class="col-md-6">
                                                        <i class="fa fa-stop-circle-o" aria-hidden="true"></i>{{ $job_same->job_ended_at }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
            </div>
        </div>
    </div>
</div>
@endsection