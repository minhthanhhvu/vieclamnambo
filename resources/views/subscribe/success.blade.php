<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />
    <title>Đăng ký nhận bản tin qua email</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/layout.css">
    <link rel="stylesheet" href="/css/select2.css">
    <link rel="stylesheet" href="/css/mobile.css">
    <link href='http://fonts.googleapis.com/css?family=Cabin' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'/>
</head>
<body>

<div id="wrapper">
    <div id="main">
        <div class="container">
            <div class="row">
                <div id="register">
                    <div class="box-head">
                        @include('shared.alert')
                    </div>

                    <div class="box-body">
                        <h3 class="text-center"><a style="color: #31708f;" href="http://vieclamnambo.vn">Quay về trang chủ</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div class="footer">
            <div class="row">
                <div class="col-md-5">
                    <a href="http://vieclamnambo.vn"><img src="http://vieclamnambo.vn/wp-content/uploads/2016/10/footerlogo.png" alt=""></a>
                    <p>© 2016 Việc Làm Nam Bộ | Công ty cổ phần Trái Thị Vàng. </p>
                    <p>Giấy phép MXH số 193/GP-BTTTT do Bộ Thông tin và Truyền thông cấp ngày 14 tháng 4 năm 2016</p>

                </div>
                <div class="col-md-7">
                    <div class="fb-page" data-href="https://www.facebook.com/vieclamnambo.vn/" data-tabs="timeline" data-width="320" data-height="90" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/vieclamnambo.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/vieclamnambo.vn/">Cổng Thông Tin Việc Làm Nam Bộ</a></blockquote></div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script src="/js/jquery-1.11.2.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>

<script>
    $(function () {
        $('.datepicker').datepicker({ format: "dd//mm/yyyy" }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
    });
</script>
</body>
</html>