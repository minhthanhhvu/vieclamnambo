<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />

    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    @yield('head')
     <link rel="stylesheet" href="/css/bootstrap.min.css">
     <link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
     <link rel="stylesheet" href="/css/layout.css">
     <link rel="stylesheet" href="/css/select2.css">
     <link rel="stylesheet" href="/css/nivo-slider.css">
     <link rel="stylesheet" href="/css/popup.css"/>
     <link rel="stylesheet" href="/css/jquery.mmenu.all.css"/>
     <link rel="stylesheet" href="/css/jquery.mCustomScrollbar.css"/>
     <link rel="stylesheet" href="/css/fileinput.min.css">
     <link rel="stylesheet" href="/css/inputShowPwd.css">
     <link rel="stylesheet" href="/css/mobile.css">
     <link rel="stylesheet" href="/css/font-awesome.min.css"/>
     <link href='http://fonts.googleapis.com/css?family=Cabin' rel='stylesheet' type='text/css'/>
     <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'/>
     <link rel="icon" href="http://vieclamnambo.vn/wp-content/uploads/2016/10/cropped-newfavicon-1-32x32.png" sizes="32x32" />
</head>

<body>
@include('shared.header')
<div id="wrapper">
    <div class="menu-mobile">
        <a href="#menu" id="menu-bottom">
            <span></span>
            <span></span>
            <span></span>
        </a>
        <a href="" class="pull-right pr_12" style="padding-right: 10px">
                <img src="/image/vlnbl-menu.svg" alt="" height="60"/>
        </a>
    </div>
    @yield('content')

    @include('shared.footer')
</div>
@include('shared.modal')

<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script src="/js/jquery-1.11.2.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/jquery.nivo.slider.js"></script>
<script src="/js/jquery.nivo.slider.pack.js"></script>
<script src="/js/jquery.mmenu.all.min.js"></script>
<script src="/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/js/fileinput.min.js"></script>
<script src="/js/inputShowPwd.js"></script>
<script src="/js/function.js"></script>
<script src="/js/ajax.js"></script>

</body>
</html>