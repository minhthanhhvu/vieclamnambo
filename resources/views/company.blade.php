@extends($layout)

@section('title', $company->meta_title)
@section('description', $company->meta_description)

@section('content')
<div id="main-top">
    <div class="container">
        <div class="main-top">
            <h3>{{ $company->name }}</h3>
            <div class="col-md-4 main-top-img">
                <img src="{!! $company->images ? $company->images : '/image/no-image.png' !!}'" title="{{ $company->name }}" alt="{{ $company->name }}" width="498" height="315">
            </div>
            <div class="col-md-5 desc">
                {!! str_limit($company->description, 400) !!}
            </div>
            <div class="col-md-3 map">
                <div class="box">
                    {!! $company->location !!}
                    {{--
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.6653029790314!2d105.7919243148278!3d21.04607399256311!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab3a9f4447a3%3A0x3c2e56b445e47c0c!2zMTQxIEhvw6BuZyBRdeG7kWMgVmnhu4d0LCBOZ2jEqWEgVMOibiwgQ-G6p3UgR2nhuqV5LCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1480728562673" width="100%" height="215" frameborder="0" style="border:0" allowfullscreen></iframe>
                    --}}
                </div>
            </div>
        </div>
    </div>
</div>
<div id="main">
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-8">
                @if ($job_lists->count() > 0)
                <div class="form-list-job">
                    <div class="box-head">
                        <h3>VIỆC LÀM MỚI NHẤT</h3>
                    </div>
                    <div class="box-body">
                        @if($job_lists->count() > 0)
                            <div class="list-jobs">
                                @foreach ($job_lists as $job_list)
                                <div class="list-item">
                                    <div class="row">
                                        <a href="/{{ $job_list->province->slug }}/{{ $job_list->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{{ $job_list->company->logo ? $job_list->company->logo : '/image/no-image.png' }}" alt="{{ $job_list->company->name }}"></a>
                                        <div class="list-item-content col-md-10 col-xs-9">
                                            <div  class="company-job"><h2><a href="/{{ $job_list->province->slug }}/{{ $job_list->industrialZone->slug }}/{{ $job_list->slug }}.html">{{ $job_list->job_title }}</a></h2>{{-- <span class="label label-danger">HOT</span> --}}</div>
                                            <div class="row">
                                                @if ($job_list->jobCarrers->count() > 0)
                                                <div class="col-md-4 mp">
                                                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                                                    @foreach ($job_list->jobCarrers as $jobCarrer)
                                                        <a href="/nganh-nghe/{{ $jobCarrer->slug }}">{{ $jobCarrer->name }}</a>
                                                    @endforeach
                                                </div>
                                                @endif
                                                <div class="col-md-4 mp">
                                                    <span><i class="fa fa-usd" aria-hidden="true"></i> {{ $job_list->jobWage ? $job_list->jobWage->name : number_format($job_list->job_wage) }}</span>
                                                </div>
                                                <div class="col-md-4 mp">
                                                    <span><i class="fa fa-stop-circle-o" aria-hidden="true"></i>{{ $job_list->job_ended_at }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="page">
                                {!! $job_lists->render() !!}
                            </div>
                        @else
                            Không tìm thấy việc làm nào trong công ty này
                        @endif
                    </div>
                </div>
                @else
                    <p>Công ty không có việc làm mới nào</p>
                @endif
            </div>
            <div id="sidebar" class="col-md-4">
                @if($industrialzone_jobs->count() > 0)
                    <div class="form-list-right">
                        <div class="box-head">
                            <h3>Việc làm khu công nghiệp</h3>
                        </div>
                        <div class="box-body">
                            @foreach ($industrialzone_jobs as $industrialzone_job)
                                <div class="list-item">
                                    <div class="row">
                                        <a href="/{{ $industrialzone_job->province->slug }}/{{ $industrialzone_job->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{!! $industrialzone_job->company->logo ? $industrialzone_job->company->logo : '/image/no-image.png' !!}" alt="{{ $industrialzone_job->company->name }}"></a>
                                        <div class=" col-md-10 col-xs-9 list-item-content">
                                            <div class="company-job"><h4><a href="/{{ $industrialzone_job->province->slug }}/{{ $industrialzone_job->industrialZone->slug }}/{{ $industrialzone_job->slug }}.html">{{ $industrialzone_job->job_title }}</a></h4><span class="label label-danger">HOT</span></div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <i class="fa fa-usd" aria-hidden="true"></i>  {{ $industrialzone_job->jobWage ? $industrialzone_job->jobWage->name : number_format($industrialzone_job->job_wage) }}
                                                </div>
                                                <div class="col-md-6">
                                                    <i class="fa fa-stop-circle-o" aria-hidden="true"></i>{{ $industrialzone_job->job_ended_at }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                {{--
                <div class="form-list-right">
                    <div class="box-head">
                        <h3>Việc làm khu công nghiệp</h3>
                    </div>
                    <div class="box-body">
                        <div class="list-item">
                            <div class="row">
                                <a href="congtythuockcn.html" class="col-md-2 col-xs-3 list-item-logo"><img src="image/0acfdcf8a2c1.jpg" alt="Logo công ty"></a>
                                <div class=" col-md-10 col-xs-9 list-item-content">
                                    <div class="company-job"><h4><a href="PostCVthuocKCN.html">Trưởng Phòng Tài Chính Kế Toán</a></h4><span class="label label-danger">MỚI</span></div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <i class="fa fa-usd" aria-hidden="true"></i>20 triệu
                                        </div>
                                        <div class="col-md-6">
                                            <i class="fa fa-stop-circle-o" aria-hidden="true"></i>20-10-2016
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                --}}
            </div>
        </div>
    </div>
</div>
@endsection