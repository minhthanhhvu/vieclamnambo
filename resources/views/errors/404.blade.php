@extends($layout)

@section('title', 'Không tìm thấy trang này')
@section('keywords', '')
@section('description', '')

@section('content')
<div class="404" style="text-align: center;font-size: 35px;color: #456; margin: 50px 0;">
    <h1>
        <img src="/image/frown.svg" alt="" width="300" /><br>
        404
        <h3 style="font-size: 30px;">Không tìm thấy trang bạn yêu cầu</h3>
    </h1>
</div>
@endsection