
@extends($layout)

@section('title', $page->meta_title)
@section('description', $page->meta_description)

@section('content')
<div id="main">
    <div class="container">
        <div class="row">
            <div id="page-detail">
                <div class="box-body">
                    <h1 class="text-center">{{ $page->name }}</h1>
                    <div class="detail" style="padding: 30px">
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection