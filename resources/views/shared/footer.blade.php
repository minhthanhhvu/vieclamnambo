<footer>
    <div class="container">
        <div class="footer">
            <div class="row">
                <div class="col-md-5">
                    <a href="#"><img src="/image/vieclamnambo.svg" alt="" height="70" class="pb_12"></a>
                    <ul class="nav-divider">
                        <li><a href="/gioi-thieu">Giới thiệu</a></li>
                        <li><a href="/huong-dan-su-dung">Hướng dẫn sử dụng</a></li>
                    </ul>
                    <p>© 2016 Việc Làm Nam Bộ | Công ty cổ phần Trái Thị Vàng. </p>
                    <p>Giấy phép MXH số 193/GP-BTTTT do Bộ Thông tin và Truyền thông cấp ngày 14 tháng 4 năm 2016</p>

                </div>
                <div class="col-md-7" style="padding:0">
                    <div class="fb-page" data-href="https://www.facebook.com/vieclamnambo.vn/" data-tabs="timeline" data-width="320" data-height="90" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/vieclamnambo.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/vieclamnambo.vn/">Cổng Thông Tin Việc Làm Nam Bộ</a></blockquote></div>
                </div>
            </div>
        </div>

        <p style="font-size:13px; margin-top: 20px">
            <a href="/viec-lam-khu-cong-nghiep-nhon-trach">Nhơn Trạch</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/viec-lam-khu-cong-nghiep-amata">AMATA</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/viec-lam-khu-cong-nghiep-phuc-long">Phúc Long</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/viec-lam-khu-cong-nghiep-thuan-dao">Thuận Đạo</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/khu-cong-nghiep-tan-phu-trung">Tân Phú Trung</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/khu-cong-nghiep-loc-an-binh-son">Bình Sơn</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/viec-lam-khu-che-xuat-tan-thuan">Tân Thuận</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/viec-lam-khu-cong-nghiep-tan-binh">Tân Bình</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/viec-lam-khu-cong-nghiep-tan-tao">Tân Tạo</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/viec-lam-khu-cong-nghiep-rach-bap">Rạch Bắp</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/viec-lam-khu-cong-nghiep-cau-tram">Cầu Tràm</a>&nbsp;&nbsp;&nbsp;&nbsp;
        </p>
    </div>
</footer>

<a class="back-to-top" href="javascript:void(0);" title="Top" style="display: inline;"></a>
<div id="fb-root"></div>

<div id="popup-login" class="modal fade" role="dialog">
    <div class="container">
        <div class="login">
            <div class="box-head">
                <h3>ĐĂNG NHẬP</h3>
            </div>
            <div class="box-body">
                <form action="" class="form-horizontal">
                    <div class="block-login">
                        <div class="form-group">
                            <label for="txtEmail" class="control-label col-md-3">Email <span style="color:red">*</span></label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" id="txtEmail" placeholder="Ví dụ: abc@gmail.com; abc@yahoo.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtPass" class="control-label col-md-3">Mật khẩu <span style="color:red">*</span></label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" id="txtPass">
                            </div>
                        </div>
                    </div>
                    <div class="block-login">
                        <div class="form-group">
                            <div class="col-md-offset-3">
                                <button class="btn">Đăng nhập</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="popup-register" class="modal fade" role="dialog">
    <div class="container">
        <div class="register">
            <ul class="nav nav-pills nav-justified">
                <li class="active"><a href="#uv" data-toggle="tab">ỨNG VIÊN</a></li>
                {{--<li><a href="#ntd" data-toggle="tab">NHÀ TUYỂN DỤNG</a></li> --}}
            </ul>
            <div class="box-body">
                <div class="tab-pane fade in active" id="uv">
                    <div class="col-sm-offset-2 col-sm-8">
                        <a href="/facebook/redirect" class="btn btn-block btn-facebook">
                            <i class="fa fa-facebook-official"></i>
                            Đăng nhập bằng Facebook
                        </a>
                        {{--
                        <a href="#" class="btn btn-block btn-google">
                            <i class="fa fa-google-plus"></i>
                            Đăng nhập bằng Google
                        </a>
                        --}}
                        <p class="text-center">hoặc</p>
                        <p class="text-center" style="font-weight:bold">Bấm vào <a href="/register" style="font-weight: bold;color:#199948">đây</a> để đăng ký</p>
                        {{-- <p class="text-center" style="font-style:italic;color:#ddd"><a href="#">Hướng dẫn đăng ký</a></p> --}}
                    </div>
                </div>
                <div class="tab-pane fade" id="ntd">
                    <div class="col-sm-offset-2 col-sm-8">
                        <a href="#" class="btn btn-block"><i class="fa fa-user-plus"></i> Đăng ký</a>
                        <br/>
                        <p class="text-center" style="font-style:italic;color:#ddd"><a href="#">Hướng dẫn đăng ký</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>