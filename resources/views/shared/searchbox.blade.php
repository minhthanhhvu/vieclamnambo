<div id="search">
    <div class="container">
        <div class="box-search-job">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        {!! Form::open(['method'=>'get', 'url'=>'tim-kiem']) !!}
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" name="key" class="form-control" placeholder="Nhập tên công việc, vị trí" value="{{ old('key') }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="career_id" id="career_id" class="form-control" style="width:100%" tabindex="-1" aria-hidden="true">
                                        <option value="">Chọn ngành nghề</option>
                                        @foreach($listbox_carrers as $listbox_carrer)
                                            <option value="{{ $listbox_carrer->id }}" {!! old('career_id')==$listbox_carrer->id ? 'selected' : '' !!}>{{ $listbox_carrer->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <select name="province_id" id="province_id" class="form-control" style="width: 100%" tabindex="-1" aria-hidden="true">
                                        <option value="">Chọn tỉnh</option>
                                        @foreach($listbox_provinces as $listbox_province)
                                            <option value="{{ $listbox_province->id }}" {!! old('province_id')==$listbox_province->id ? 'selected' : '' !!}>{{ $listbox_province->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="industrial_zone_id" id="industrial_zone_id" class="form-control" id="kcn" style="width: 100%" tabindex="-1" aria-hidden="true" {{ old('province_id') ? '' : 'disabled' }}>
                                        <option value="">Chọn khu công nghiệp</option>
                                        @if (old('province_id'))
                                            @foreach (LoadView::industrialZones(old('province_id')) as $industrial_zone)
                                                <option value="{{ $industrial_zone->id }}" {!! old('industrial_zone_id')==$industrial_zone->id ? 'selected' : '' !!}>{{ $industrial_zone->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-green btn-vlnb"><i class="glyphicon glyphicon-search" aria-hidden="true"></i> Tìm</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>