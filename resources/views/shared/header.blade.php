<header id="desktop">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1><a href="/" class="navbar-brand"><img src="/image/vieclamnambo.svg" alt=""></a></h1>
            </div>


            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/viec-lam-o-binh-duong">Bình Dương<span class="caret"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-song-than-1">Sóng Thần 1 </a></li>
                            <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-song-than-2">Sóng Thần 2 </a></li>
                            <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-vsip-1">VSIP 1</a></li>
                            <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-nam-tan-uyen">Nam Tân Uyên</a></li>
                            <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-nam-tan-uyen-mo-rong">Nam Tân Uyên mở rộng</a></li>
                            <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-song-than-3">Sóng Thần 3</a></li>
                            <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-viet-huong-1">Việt Hương 1</a></li>
                            <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-rach-bap">Rạch Bắp</a></li>
                            <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-vsip-2">VSIP 2</a></li>
                            <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-bau-bang">Bàu Bàng</a></li>
                        </ul>
                    </li>
                    <li><a href="/viec-lam-o-dong-nai">Đồng Nai<span class="caret"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-nhon-trach-2">Nhơn Trạch 2</a></li>
                            <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-nhon-trach-1">Nhơn Trạch 1</a></li>
                            <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-bien-hoa-2">Biên Hòa 2</a></li>
                            <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-loc-an-binh-son">Lộc An - Bình Sơn</a></li>
                            <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-loteco">Loteco</a></li>
                            <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-bien-hoa-1">Biên Hòa 1</a></li>
                            <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-amata">AMATA</a></li>
                            <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-nhon-trach-3">Nhơn Trạch 3</a></li>
                            <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-long-thanh">Long Thành</a></li>
                            <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-go-dau">Gò Dầu</a></li>
                        </ul>
                    </li>
                    <li><a href="/viec-lam-o-vung-tau">Vũng Tàu<span class="caret"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-phu-my-2">Phú Mỹ 2</a></li>
                            <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-phu-my-1">Phú Mỹ 1</a></li>
                            <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-dong-xuyen">Đông Xuyên</a></li>
                            <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-my-xuan-a1">Mỹ Xuân A1</a></li>
                            <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-my-xuan-b1">Mỹ Xuân B1</a></li>
                            <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-my-xuan-a2">Mỹ Xuân A2</a></li>
                        </ul>
                    </li>
                    <li><a href="/viec-lam-o-sai-gon">Sài Gòn<span class="caret"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-cat-lai">Cát Lái </a></li>
                            <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-tan-tao">Tân Tạo</a></li>
                            <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-tan-binh">Tân Bình</a></li>
                            <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-tan-phu-trung">Tân Phú Trung </a></li>
                            <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-che-xuat-tan-thuan">Tân Thuận</a></li>
                            <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-tay-bac-cu-chi">Tây Bắc Củ Chi</a></li>
                            <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghe-cao-ho-chi-minh">Hồ Chí Minh</a></li>
                            <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-hiep-phuoc">Hiệp Phước</a></li>
                            <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-che-xuat-linh-trung-1">Linh Trung 1</a></li>
                            <li><a href="/viec-lam-o-sai-gon/viec-lam-cum-cong-nghiep-xuan-thoi-son">Xuân Thới Sơn</a></li>
                            <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-che-xuat-linh-trung-ii">Linh Trung II</a></li>
                        </ul>
                    </li>
                    <li><a href="/viec-lam-o-long-an">Long An<span class="caret"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-cau-tram">Cầu Tràm</a></li>
                            <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-thuan-dao">Thuận Đạo</a></li>
                            <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-thuan-dao-mo-rong">Thuận Đạo Mở Rộng</a></li>
                            <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-long-hau">Long Hậu</a></li>
                            <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-phuc-long">Phúc Long</a></li>
                            <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-long-cang-long-dinh">Long Cang - Long Định</a></li>
                            <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-loi-binh-nhon">Lợi Bình Nhơn</a></li>
                            <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-vinh-loc-2-long-an">Vĩnh Lộc 2- Long An</a></li>
                            <li><a href="/viec-lam-o-long-an/viec-lam-kcn-nhut-chanh">NHỰT CHÁNH</a></li>
                            <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-thinh-phat">Thịnh Phát</a></li>
                        </ul>
                    </li>
                    <li><a href="/viec-lam-o-tien-giang">Tiền Giang<span class="caret"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="/viec-lam-o-tien-giang/viec-lam-khu-cong-nghiep-tan-huong">Tân Hương</a></li>
                            <li><a href="/viec-lam-o-tien-giang/viec-lam-khu-cong-nghiep-my-tho">Mỹ Tho</a></li>
                            <li><a href="/viec-lam-o-tien-giang/viec-lam-khu-cong-nghiep-long-giang">Long Giang</a></li>
                            <li><a href="/viec-lam-o-tien-giang/viec-lam-cum-cong-nghiep-tan-my-chanh">Tân Mỹ Chánh</a></li>
                            <li><a href="/viec-lam-o-tien-giang/viec-lam-cum-cong-nghiep-trung-an">Trung An</a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="/login"  data-toggle="modal">Đăng nhập&nbsp;<span class="fa fa-user"></span></a></li>
                        {{-- <li><a href="javascript:void(0)"  data-toggle="modal" data-target="#popup-login">Đăng nhập&nbsp;<span class="fa fa-user"></span></a></li> --}}
                        <li><a href="javascript:void(0)"  data-toggle="modal" data-target="#popup-register">Đăng ký&nbsp;<span class="fa fa-user-plus"></span></a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->fullname }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/thong-tin-ca-nhan">Thông tin cá nhân</a></li>
                                <li><a href="/doi-mat-khau">Đổi mật khẩu</a></li>
                                <li><a href="/viec-lam-da-luu">Việc làm đã lưu</a></li>
                                <li>
                                    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Đăng xuất
                                    </a>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div><!--Container-->
    </nav>
</header>

<header id="mobile">
    <nav id="menu">
        <ul class="nav navbar-nav">
            <li><a href="/viec-lam-o-binh-duong">Bình Dương</a>
                <ul class="list-unstyled">
                    <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-song-than-1">Sóng Thần 1 </a></li>
                    <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-song-than-2">Sóng Thần 2 </a></li>
                    <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-vsip-1">VSIP 1</a></li>
                    <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-nam-tan-uyen">Nam Tân Uyên</a></li>
                    <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-nam-tan-uyen-mo-rong">Nam Tân Uyên mở rộng</a></li>
                    <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-song-than-3">Sóng Thần 3</a></li>
                    <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-viet-huong-1">Việt Hương 1</a></li>
                    <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-rach-bap">Rạch Bắp</a></li>
                    <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-vsip-2">VSIP 2</a></li>
                    <li><a href="/viec-lam-o-binh-duong/viec-lam-khu-cong-nghiep-bau-bang">Bàu Bàng</a></li>
                </ul>
            </li>
            <li><a href="/viec-lam-o-dong-nai">Đồng Nai</a>
                <ul class="list-unstyled">
                    <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-nhon-trach-2">Nhơn Trạch 2</a></li>
                    <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-nhon-trach-1">Nhơn Trạch 1</a></li>
                    <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-bien-hoa-2">Biên Hòa 2</a></li>
                    <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-loc-an-binh-son">Lộc An - Bình Sơn</a></li>
                    <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-loteco">Loteco</a></li>
                    <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-bien-hoa-1">Biên Hòa 1</a></li>
                    <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-amata">AMATA</a></li>
                    <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-nhon-trach-3">Nhơn Trạch 3</a></li>
                    <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-long-thanh">Long Thành</a></li>
                    <li><a href="/viec-lam-o-dong-nai/viec-lam-khu-cong-nghiep-go-dau">Gò Dầu</a></li>
                </ul>
            </li>
            <li><a href="/viec-lam-o-vung-tau">Vũng Tàu</a>
                <ul class="list-unstyled">
                    <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-phu-my-2">Phú Mỹ 2</a></li>
                    <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-phu-my-1">Phú Mỹ 1</a></li>
                    <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-dong-xuyen">Đông Xuyên</a></li>
                    <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-my-xuan-a1">Mỹ Xuân A1</a></li>
                    <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-my-xuan-b1">Mỹ Xuân B1</a></li>
                    <li><a href="/viec-lam-o-vung-tau/viec-lam-khu-cong-nghiep-my-xuan-a2">Mỹ Xuân A2</a></li>
                </ul>
            </li>
            <li><a href="/viec-lam-o-sai-gon">Sài Gòn</a>
                <ul class="list-unstyled">
                    <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-cat-lai">Cát Lái </a></li>
                    <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-tan-tao">Tân Tạo</a></li>
                    <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-tan-binh">Tân Bình</a></li>
                    <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-tan-phu-trung">Tân Phú Trung </a></li>
                    <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-che-xuat-tan-thuan">Tân Thuận</a></li>
                    <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-tay-bac-cu-chi">Tây Bắc Củ Chi</a></li>
                    <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghe-cao-ho-chi-minh">Hồ Chí Minh</a></li>
                    <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-cong-nghiep-hiep-phuoc">Hiệp Phước</a></li>
                    <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-che-xuat-linh-trung-1">Linh Trung 1</a></li>
                    <li><a href="/viec-lam-o-sai-gon/viec-lam-cum-cong-nghiep-xuan-thoi-son">Xuân Thới Sơn</a></li>
                    <li><a href="/viec-lam-o-sai-gon/viec-lam-khu-che-xuat-linh-trung-ii">Linh Trung II</a></li>
                </ul>
            </li>
            <li><a href="/viec-lam-o-long-an">Long An</a>
                <ul class="list-unstyled">
                    <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-cau-tram">Cầu Tràm</a></li>
                    <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-thuan-dao">Thuận Đạo</a></li>
                    <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-thuan-dao-mo-rong">Thuận Đạo Mở Rộng</a></li>
                    <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-long-hau">Long Hậu</a></li>
                    <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-phuc-long">Phúc Long</a></li>
                    <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-long-cang-long-dinh">Long Cang - Long Định</a></li>
                    <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-loi-binh-nhon">Lợi Bình Nhơn</a></li>
                    <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-vinh-loc-2-long-an">Vĩnh Lộc 2- Long An</a></li>
                    <li><a href="/viec-lam-o-long-an/viec-lam-kcn-nhut-chanh">NHỰT CHÁNH</a></li>
                    <li><a href="/viec-lam-o-long-an/viec-lam-khu-cong-nghiep-thinh-phat">Thịnh Phát</a></li>
                </ul>
            </li>
            <li><a href="/viec-lam-o-tien-giang">Tiền Giang</a>
                <ul class="list-unstyled">
                    <li><a href="/viec-lam-o-tien-giang/viec-lam-khu-cong-nghiep-tan-huong">Tân Hương</a></li>
                    <li><a href="/viec-lam-o-tien-giang/viec-lam-khu-cong-nghiep-my-tho">Mỹ Tho</a></li>
                    <li><a href="/viec-lam-o-tien-giang/viec-lam-khu-cong-nghiep-long-giang">Long Giang</a></li>
                    <li><a href="/viec-lam-o-tien-giang/viec-lam-cum-cong-nghiep-tan-my-chanh">Tân Mỹ Chánh</a></li>
                    <li><a href="/viec-lam-o-tien-giang/viec-lam-cum-cong-nghiep-trung-an">Trung An</a></li>
                </ul>
            </li>
         </ul>
        <div id="menu-header">
                <a href="/login" class="col-xs-6">
                    <i class="fa fa-user"></i>
                    Đăng nhập
                </a>
                <a href="/register" class="col-xs-6">
                    <i class="fa fa-user-plus"></i>
                    Đăng ký
                </a>
        </div>
        <div id="menu-footer">
            <a href="/" class="logo"><img src="/image/vieclamnambo.svg" alt="" height="50"/></a>
        </div>
    </nav>
</header>
