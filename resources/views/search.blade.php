@extends($layout)

@section('title', 'Tìm kiếm việc làm')
@section('keywords', '')
@section('description', '')

@section('content')
@include('shared.searchbox')
<div id="main">
	<div class="container">
		<div class="row">
			<div id="content" class="col-md-8">
				<div class="form-list-job">
					<div class="box-head">
						<h3>Kết quả tìm kiếm</h3>
					</div>
					<div class="paging-meta">
						Tìm thấy <span style="color:#ff6116; font-weight: bold">{{ number_format($job_lists->total()) }}</span> việc làm
						@if($key)
							với từ khóa <strong>"{{ $key }}"</strong>
						@endif

						@if($carrer)
							ngành <strong>{{ $carrer->name }}</strong>
						@endif

						@if($province)
							@if($industrial_zone)
								tại <strong>{{ $industrial_zone->name }}</strong> tỉnh <strong>{{ $province->name }}</strong>
							@else
								tại <strong>{{ $province->name }}</strong>
							@endif
						@endif
					</div>
					<div class="box-body">
						<div class="list-jobs">
							@foreach($job_lists as $job_list)
							<div class="list-item">
								<div class="row">
									<a href="/{{ $job_list->province->slug }}/{{ $job_list->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{!! $job_list->company->logo ? $job_list->company->logo : '/image/no-image.png' !!}" alt="{{ $job_list->company->name }}"></a>
									<div class="list-item-content col-md-10 col-xs-9">
										<div class="company-job"><h2><a href="/{{$job_list->province->slug }}/{{$job_list->industrialZone->slug }}/{{$job_list->slug }}.html">{{ $job_list->job_title }}</a></h2></div>
										<div class="row">
											<div class="col-md-4 mp">
												@if ($job_list->jobCarrers)
													<i class="fa fa-suitcase" aria-hidden="true"></i>
													@foreach ($job_list->jobCarrers as $jobCarrer)
														<a href="/nganh-nghe/{{ $jobCarrer->slug }}">{{ $jobCarrer->name }}</a>
													@endforeach
												@endif
											</div>
											<div class="col-md-4 mp">
												<span><i class="fa fa-usd" aria-hidden="true"></i>{{ $job_list->jobWage ? $job_list->jobWage->name : number_format($job_list->job_wage) }}</span>
											</div>
											<div class="col-md-4 mp">
												<span><i class="fa fa-stop-circle-o" aria-hidden="true"></i>{{ $job_list->job_ended_at }}</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						<div class="page">
							{!!	$job_lists->render() !!}
						</div>
					</div>
				</div>
			</div>

			<div id="sidebar" class="col-md-4">
				@if ($hot_job)
					<div class="job-one">
						<div class="box-head">
							<h3>Việc làm nổi bật</h3>
						</div>
						<div class="box-body">
							<div class="company-job"><h4><a href="/{{ $hot_job->province->slug }}/{{ $hot_job->industrialZone->slug }}/{{ $hot_job->slug }}.html">{{ $hot_job->job_title }}</a><span class="label label-danger">MỚI</span></h4></div>
							@if($hot_job->company) <p><i class="fa fa-address-book" aria-hidden="true"></i><a href="/{{ $hot_job->province->slug }}/{{ $hot_job->company->slug }}">{{ $hot_job->company->name }}</a></p> @endif
							<p class="block-50"><span  data-toggle="tooltip" title="{{ $hot_job->province->name }}" style="position: relative"><i class="fa fa-map-marker" aria-hidden="true"></i><a href="/{{ $hot_job->province->slug }}">{{ $hot_job->province->name }}</a></span></p>
							<p class="block-50"><span  data-toggle="tooltip" title=" {{ $hot_job->jobWage ? $hot_job->jobWage->name : number_format($hot_job->job_wage) }}" style="position: relative"><i class="fa fa-usd" aria-hidden="true"></i>  {{ $hot_job->jobWage ? $hot_job->jobWage->name : number_format($hot_job->job_wage) }}</span></p>
							<p class="block-50"><span  data-toggle="tooltip" title="{{ $hot_job->jobExperience->name }}" style="position: relative"><i class="fa fa-user-secret" aria-hidden="true"></i>{{ $hot_job->jobExperience->name }}</span></p>
							<p class="block-50"><span  data-toggle="tooltip" title="{{ $hot_job->job_ended_at }}" style="position: relative"><i class="fa fa-stop-circle-o" aria-hidden="true"></i></i>{{ $hot_job->job_ended_at }}</span></p>
							<p style="DISPLAY: -webkit-inline-box;">{!! str_limit($hot_job->description, 200) !!}</p>
							<a href="/{{ $hot_job->province->slug }}/{{ $hot_job->industrialZone->slug }}/{{ $hot_job->slug }}.html" class="more">Xem chi tiết</a>
						</div>
					</div>
				@endif

				@if ($recuit_job)
					<div class="job-one">
						<div class="box-head">
							<h3>Việc làm tuyển gấp</h3>
						</div>
						<div class="box-body">
							<div class="company-job"><h4><a href="/{{ $recuit_job->province->slug }}/{{ $recuit_job->industrialZone->slug }}/{{ $recuit_job->slug }}.html">{{ $recuit_job->job_title }}</a><span class="label label-danger">GẤP</span></h4></div>
							<p><i class="fa fa-address-book" aria-hidden="true"></i><a href="/{{ $recuit_job->province->slug }}/{{ $recuit_job->company->slug }}">{{ $recuit_job->company->name }}</a></p>
							<p class="block-50"><span  data-toggle="tooltip" title="{{ $recuit_job->province->name }}" style="position: relative"><i class="fa fa-map-marker" aria-hidden="true"></i><a href="{{ $recuit_job->province->slug }}">{{ $recuit_job->province->name }}</a></span></p>
							<p class="block-50"><span  data-toggle="tooltip" title="{{ $recuit_job->jobWage ? $recuit_job->jobWage->name : number_format($recuit_job->job_wage) }}" style="position: relative"><i class="fa fa-usd" aria-hidden="true"></i>{{ $recuit_job->jobWage ? $recuit_job->jobWage->name : number_format($recuit_job->job_wage) }}</span></p>
							<p class="block-50"><span  data-toggle="tooltip" title="{{ $recuit_job->jobExperience->name }}" style="position: relative"><i class="fa fa-user-secret" aria-hidden="true"></i>{{ $recuit_job->jobExperience->name }}</span></p>
							<p class="block-50"><span  data-toggle="tooltip" title="{{ $recuit_job->job_ended_at }}" style="position: relative"><i class="fa fa-stop-circle-o" aria-hidden="true"></i></i>{{ $recuit_job->job_ended_at }}</span></p>
							<p>{!! str_limit($recuit_job->description, 150) !!}</p>
							<a href="/{{ $recuit_job->province->slug }}/{{ $recuit_job->industrialZone->slug }}/{{ $recuit_job->slug }}" class="more">Xem chi tiết</a>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection