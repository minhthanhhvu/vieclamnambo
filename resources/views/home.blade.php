@extends($layout)

@section('title', 'Việc Làm Nam Bộ | Cổng thông tin việc làm hàng đầu khu vực phía Nam')
@section('description', '')

@section('content')
    @include('shared.searchbox')
    <div id="category">
        <div class="container">
            <div class="category">
                <div class="box-head">
                    <h3>Việc làm theo ngành nghề</h3>
                </div>
                <div class="box-body">
                    <div class="form-category">
                        <div class="row">
                            @foreach($job_carrers as $job_carrer)
                                <a class="category-single col-md-3 col-xs-6" href="/nganh-nghe/{{ $job_carrer->slug }}">
                                    <img class="salesLogoImage" src="{!! $job_carrer->icon ? $job_carrer->icon : '/image/no-image.png' !!}" >
                                    <p class="text-center category-name">{{ $job_carrer->name }}</p>
                                    {{--
                                    <p class="text-center category-job">
                                        ( <span style="color:red">{{ number_format($job_carrer->jobLists->count()) }}</span> việc làm mới )
                                    </p>
                                    --}}
                                </a>
                            @endforeach

                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                <a href="/nganh-nghe" class="btn more">Hiển thị thêm</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="main">
        <div class="container">
            <div class="row">
                <div id="content" class="col-md-8">
                    <div class="box-body">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <h3><a data-toggle="pill" href="#result1">Việc làm mới nhất</a></h3>
                            </li>
                            <li>
                                <h3><a data-toggle="pill" href="#result2">Việc làm hấp dẫn</a></h3>
                            </li>
                        </ul>
                        {{--
                        <div class="paging-meta">
                            <marquee onmouseover="this.stop()" onmouseout="this.start()" scrollamount="4">
                                @foreach ($new_job_provinces as $new_job_province)
                                    @if($new_job_province->industrialZones)
                                        {{ $new_job_province->name }}:
                                        @foreach ($new_job_province->industrialZones as $industrialZone)
                                            {{ $industrialZone->name }} (<span style="color:red">{{ $industrialZone->jobLists->count() }}</span>),
                                        @endforeach
                                    @endif
                                @endforeach
                            </marquee>
                        </div>
                        --}}
                        <div class="result">
                            <div id="result1" class="tab-pane fade in active">
                                @foreach ($new_job_lists as $new_job_list)
                                    <div class="list-item">
                                        <div class="row">
                                            <a href="/{{ $new_job_list->province->slug }}/{{ $new_job_list->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{!! $new_job_list->company->logo ? $new_job_list->company->logo : '/image/no-image.png' !!}" alt="{{ $new_job_list->company->name }}"></a>
                                            <div class="list-item-content col-md-10 col-xs-9">
                                                <div class="company-job"><h2><a href="/{{ $new_job_list->province->slug }}/{{ $new_job_list->industrialZone->slug }}/{{ $new_job_list->slug }}.html">{{ $new_job_list->job_title }}</a>{!! $new_job_list->checkHot() !!}</h2></div>

                                                <div class="row">
                                                    <div class="col-md-4 mp">
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i><a href="/{{ $new_job_list->province->slug }}">{{ $new_job_list->province->name }}</a>
                                                    </div>
                                                    <div class="col-md-4 mp">
                                                        <span><i class="fa fa-usd" aria-hidden="true"></i> {{ $new_job_list->jobWage ? $new_job_list->jobWage->name : number_format($new_job_list->job_wage) }}</span>
                                                    </div>
                                                    <div class="col-md-4 mp">
                                                        <span><i class="fa fa-stop-circle-o" aria-hidden="true"></i></i>{{ $new_job_list->job_ended_at }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div id="result2" class="tab-pane fade">
                                @foreach ($high_job_lists as $high_job_lists)
                                    <div class="list-item">
                                        <div class="row">
                                            <a href="/{{ $high_job_lists->province->slug }}/{{ $high_job_lists->company->slug }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{!! $high_job_lists->company->logo ? $high_job_lists->company->logo : '/image/no-image.png' !!}" alt="{{ $high_job_lists->company->name }}"></a>
                                            <div class="list-item-content col-md-10 col-xs-9">
                                                <div class="company-job"><h2><a href="/{{ $high_job_lists->province->slug }}/{{ $high_job_lists->industrialZone->slug }}/{{ $high_job_lists->slug }}.html">{{ $high_job_lists->job_title }}</a></h2></div>

                                                <div class="row">
                                                    <div class="col-md-4 mp">
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i><a href="/{{ $high_job_lists->province->slug }}">{{ $high_job_lists->province->name }}</a>
                                                    </div>
                                                    <div class="col-md-4 mp">
                                                        <span><i class="fa fa-usd" aria-hidden="true"></i> {{ $new_job_list->jobWage ? $new_job_list->jobWage->name : number_format($new_job_list->job_wage) }}</span>
                                                    </div>
                                                    <div class="col-md-4 mp">
                                                        <span><i class="fa fa-stop-circle-o" aria-hidden="true"></i>{{ $high_job_lists->job_ended_at }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div id="sidebar" class="col-md-4">
                    @if ($hot_job)
                        <div class="job-one">
                            <div class="box-head">
                                <h3>Việc làm nổi bật</h3>
                            </div>
                            <div class="box-body">
                                <div class="company-job"><h4><a href="/{{ $hot_job->province->slug }}/{{ $hot_job->industrialZone->slug }}/{{ $hot_job->slug }}.html">{{ $hot_job->job_title }}</a><span class="label label-danger">MỚI</span></h4></div>
                                <p><i class="fa fa-address-book" aria-hidden="true"></i><a href="/{{ $hot_job->province->slug }}/{{ $hot_job->company->slug }}">{{ $hot_job->company->name }}</a></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $hot_job->province->name }}" style="position: relative"><i class="fa fa-map-marker" aria-hidden="true"></i><a href="/{{ $hot_job->province->slug }}">{{ $hot_job->province->name }}</a></span></p>
                                <p class="block-50"><span  data-toggle="tooltip" title=" {{ $hot_job->jobWage ? $hot_job->jobWage->name : number_format($hot_job->job_wage) }}" style="position: relative"><i class="fa fa-usd" aria-hidden="true"></i>  {{ $hot_job->jobWage ? $hot_job->jobWage->name : number_format($hot_job->job_wage) }}</span></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $hot_job->jobExperience->name }}" style="position: relative"><i class="fa fa-user-secret" aria-hidden="true"></i>{{ $hot_job->jobExperience->name }}</span></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $hot_job->job_ended_at }}" style="position: relative"><i class="fa fa-stop-circle-o" aria-hidden="true"></i></i>{{ $hot_job->job_ended_at }}</span></p>
                                <p style="DISPLAY: -webkit-inline-box;">{!! str_limit($hot_job->description, 200) !!}</p>
                                <a href="/{{ $hot_job->province->slug }}/{{ $hot_job->industrialZone->slug }}/{{ $hot_job->slug }}.html" class="more">Xem chi tiết</a>
                            </div>
                        </div>
                    @endif

                    @if ($recuit_job)
                        <div class="job-one">
                            <div class="box-head">
                                <h3>Việc làm tuyển gấp</h3>
                            </div>
                            <div class="box-body">
                                <div class="company-job"><h4><a href="/{{ $recuit_job->province->slug }}/{{ $recuit_job->industrialZone->slug }}/{{ $recuit_job->slug }}.html">{{ $recuit_job->job_title }}</a><span class="label label-danger">GẤP</span></h4></div>
                                <p><i class="fa fa-address-book" aria-hidden="true"></i><a href="/{{ $recuit_job->province->slug }}/{{ $recuit_job->company->slug }}">{{ $recuit_job->company->name }}</a></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $recuit_job->province->name }}" style="position: relative"><i class="fa fa-map-marker" aria-hidden="true"></i><a href="/{{ $recuit_job->province->slug }}">{{ $recuit_job->province->name }}</a></span></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $recuit_job->jobWage ? $recuit_job->jobWage->name : number_format($recuit_job->job_wage) }}" style="position: relative"><i class="fa fa-usd" aria-hidden="true"></i>{{ $recuit_job->jobWage ? $recuit_job->jobWage->name : number_format($recuit_job->job_wage) }}</span></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $recuit_job->jobExperience->name }}" style="position: relative"><i class="fa fa-user-secret" aria-hidden="true"></i>{{ $recuit_job->jobExperience->name }}</span></p>
                                <p class="block-50"><span  data-toggle="tooltip" style="position: relative"><i class="fa fa-stop-circle-o" aria-hidden="true"></i></i>Còn <font color="red">{{ $recuit_job->subJobEndedAt() }}</font> ngày</span></p>
                                <p>{!! str_limit($recuit_job->description, 150) !!}</p>
                                <a href="/{{ $recuit_job->province->slug }}/{{ $recuit_job->industrialZone->slug }}/{{ $recuit_job->slug }}.html" class="more">Xem chi tiết</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--<div id="post-new">
        <div class="container">
            <div class="post-new">
                <div class="box-head">
                    <h3>
                        Cẩm nang - tin tức
                    </h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="post">
                                <a href="#"><img src="/image/blog-post-03-498x315.jpg" alt=""></a>
                                <h2><a href="#">11 Tips to Help You Get New Clients Through Cold Calling</a></h2>
                                <p>
                                    Objectively innovate empowered manufactured products whereas parallel platforms. Holisticly predominate
                                </p>
                                <a href="#" class="btn">Đọc thêm</a>
                            </div>

                        </div>
                        <div class="col-xs-4">
                            <div class="post">
                                <a href="#"><img src="/image/blog-post-03-498x315.jpg" alt=""></a>
                                <h2><a href="#">11 Tips to Help You Get New Clients Through Cold Calling</a></h2>
                                <p>
                                    Objectively innovate empowered manufactured products whereas parallel platforms. Holisticly predominate
                                </p>
                                <a href="#" class="btn">Đọc thêm</a>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="post">
                                <a href="#"><img src="/image/blog-post-03-498x315.jpg" alt=""></a>
                                <h2><a href="#">11 Tips to Help You Get New Clients Through Cold Calling</a></h2>
                                <p>
                                    Objectively innovate empowered manufactured products whereas parallel platforms. Holisticly predominate
                                </p>
                                <a href="#" class="btn">Đọc thêm</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
@endsection
