@extends($layout)

@section('title', 'Xem thông tin cá nhân | Việc Làm Nam Bộ | Cổng thông tin việc làm hàng đầu khu vực phía Nam')
@section('description', '')

@section('content')
<div class="container">
    <div class="row" style="margin-top: 30px;">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Đăng ký</div>
                <div class="panel-body">
                    @include('shared.alert')
                    {!! Form::open(['method' => 'post', 'url' => '/thong-tin-ca-nhan', 'enctype' => 'multipart/form-data']) !!}
                    {{--
                    <div class="form-group">
                        {!! Form::label('avatar', 'Avatar') !!}
                        <p></p><img src="{{ $user_custom->avatar ? $user_custom->avatar : '/image/no-avatar.jpg' }}" alt="" height="100"></p>
                        <p>{!! Form::file('avatar') !!}</p>
                    </div>
                    --}}

                    <div class="form-group">
                        {!! Form::label('fullname', 'Họ tên *') !!}
                        {!! Form::text('fullname', $user_custom->fullname, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('gender', 'Giới tính *') !!}
                        {!! Form::select('gender', ['' => 'Chọn giới tính', '0' => 'Nữ', '1' => 'Nam'], $user_custom->gender, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('province', 'Tỉnh / Thành phố mong muốn làm việc *') !!}
                        {!! Form::select('province', $provinces, $user_custom->province, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('address', 'Địa chỉ *') !!}
                        {!! Form::text('address', $user_custom->address, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('birthday', 'Ngày sinh *') !!}
                        {!! Form::text('birthday', $user_custom->birthday, ['class'=>'form-control datepicker', 'placeholder' => 'Ngày sinh có định dạng ngày/tháng/năm']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', 'Số điện thoại *') !!}
                        {!! Form::text('phone', $user_custom->phone, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'Email *') !!}
                        {!! Form::text('email', $user_custom->email, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Cập nhật thông tin', ['class'=>'btn btn-primary']) !!}
                        {!! Form::reset('Hủy cập nhật', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection