@extends($layout)

@section('title', 'Xem thông tin cá nhân | Việc Làm Nam Bộ | Cổng thông tin việc làm hàng đầu khu vực phía Nam')
@section('description', '')

@section('content')
    <div class="container">
        <div class="row" style="margin-top: 30px;">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Đổi mật khẩu</div>
                    <div class="panel-body">
                        @include('shared.alert')
                        {!! Form::open(['method' => 'post', 'url' => '/doi-mat-khau']) !!}

                        <div class="form-group">
                            {!! Form::label('old_password', 'Mật khẩu cũ') !!}
                            {!! Form::password('old_password', ['class'=>'form-control', 'placeholder' => 'Nếu đăng nhập bằng mạng xã hội thì bỏ trống trường này']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'Mật khẩu mới') !!}
                            {!! Form::password('password', ['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password-confirm', 'Xác nhận mật khẩu mới') !!}
                            {!! Form::password('password_confirmation', ['class'=>'form-control', 'id' => 'password-confirm']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Đổi mật khẩu', ['class'=>'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection