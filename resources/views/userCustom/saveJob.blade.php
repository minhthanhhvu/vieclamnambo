@extends($layout)

@section('title', 'Việc làm đã lưu | Việc Làm Nam Bộ | Cổng thông tin việc làm hàng đầu khu vực phía Nam')
@section('description', '')

@section('content')
    <div id="main">
        <div class="container">
            <div class="row">
                <div id="content" class="col-md-8">
                    @include('shared.alert')
                    <div class="paging-result">
                        <h3>VIỆC LÀM ĐÃ LƯU ( <span style="color:red">{{ $custom_save_jobs->total() }}</span>)</h3>
                    </div>
                    <div class="box-body">
                        @if ($custom_save_jobs->count() > 0)
                        {!! Form::open(['method' => 'post', 'url' => '/viec-lam-da-luu/xoa']) !!}
                        <div class="list-jobs">
                            @foreach($custom_save_jobs as $custom_save_job)
                            <div class="list-item">
                                <div class="row">
                                    <a href="/{{ $custom_save_job->jobList->province->slug }}/{{ $custom_save_job->jobList->company->slug }}" title="{{ $custom_save_job->jobList->company->name }}" class="col-md-2 col-xs-3 list-item-logo"><img src="{{ $custom_save_job->jobList->company->logo ? $custom_save_job->jobList->company->logo : '/image/no-image.png' }}" alt="{{ $custom_save_job->jobList->company->name }}"></a>
                                    <div class="list-item-content col-md-10 col-xs-9">
                                        <div  class="company-job"><h2><a href="/{{ $custom_save_job->jobList->province->slug }}/{{ $custom_save_job->jobList->industrialZone->slug }}/{{ $custom_save_job->jobList->slug }}.html">{{ $custom_save_job->jobList->job_title }}</a></h2>{{-- <span class="label label-danger">HOT</span> --}}</div>
                                        <div class="row">
                                            <div class="col-md-4 mp">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i><a href="/{{ $custom_save_job->jobList->province->slug }}">{{ $custom_save_job->jobList->province->name }}</a>
                                            </div>
                                            <div class="col-md-3 mp">
                                                <span><i class="fa fa-usd" aria-hidden="true"></i>{{ $custom_save_job->jobList->jobWage ? $custom_save_job->jobList->jobWage->name : number_format($custom_save_job->jobList->job_wage) }}</span>
                                            </div>
                                            <div class="col-md-4 mp">
                                                <span><i class="fa fa-stop-circle-o" aria-hidden="true"></i>{{ $custom_save_job->jobList->job_ended_at ? $custom_save_job->jobList->job_ended_at : 'Hết hạn' }}</span>
                                            </div>
                                            <div class="col-md-1 mp">
                                                <div class="check-job">
                                                    <input type="checkbox" value="{{ $custom_save_job->id }}" id="check-job-{{ $custom_save_job->id }}" name="custom_save_job_ids[]" />
                                                    <label for="check-job-{{ $custom_save_job->id }}"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="page">
                            <div class=" col-xs-10">
                                {{ $custom_save_jobs->render() }}
                            </div>
                            <div class="col-xs-2">
                                <button type="submit" class="btn">Xóa</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        @else
                            <p>Không kết quả nào được tìm thấy</p>
                        @endif
                    </div>
                </div>
                <div id="sidebar" class="col-md-4">
                    @if ($hot_job)
                        <div class="job-one">
                            <div class="box-head">
                                <h3>Việc làm nổi bật</h3>
                            </div>
                            <div class="box-body">
                                <div class="company-job"><h4><a href="/{{ $hot_job->province->slug }}/{{ $hot_job->industrialZone->slug }}/{{ $hot_job->slug }}.html">{{ $hot_job->job_title }}</a><span class="label label-danger">MỚI</span></h4></div>
                                <p><i class="fa fa-address-book" aria-hidden="true"></i><a href="/{{ $hot_job->province->slug }}/{{ $hot_job->company->slug }}">{{ $hot_job->company->name }}</a></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $hot_job->province->name }}" style="position: relative"><i class="fa fa-map-marker" aria-hidden="true"></i><a href="/{{ $hot_job->province->slug }}">{{ $hot_job->province->name }}</a></span></p>
                                <p class="block-50"><span  data-toggle="tooltip" title=" {{ $hot_job->jobWage ? $hot_job->jobWage->name : number_format($hot_job->job_wage) }}" style="position: relative"><i class="fa fa-usd" aria-hidden="true"></i>  {{ $hot_job->jobWage ? $hot_job->jobWage->name : number_format($hot_job->job_wage) }}</span></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $hot_job->jobExperience->name }}" style="position: relative"><i class="fa fa-user-secret" aria-hidden="true"></i>{{ $hot_job->jobExperience->name }}</span></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $hot_job->job_ended_at }}" style="position: relative"><i class="fa fa-stop-circle-o" aria-hidden="true"></i></i>{{ $hot_job->job_ended_at }}</span></p>
                                <p style="DISPLAY: -webkit-inline-box;">{!! str_limit($hot_job->description, 200) !!}</p>
                                <a href="/{{ $hot_job->province->slug }}/{{ $hot_job->industrialZone->slug }}/{{ $hot_job->slug }}.html" class="more">Xem chi tiết</a>
                            </div>
                        </div>
                    @endif

                    @if ($recuit_job)
                        <div class="job-one">
                            <div class="box-head">
                                <h3>Việc làm tuyển gấp</h3>
                            </div>
                            <div class="box-body">
                                <div class="company-job"><h4><a href="/{{ $recuit_job->province->slug }}/{{ $recuit_job->industrialZone->slug }}/{{ $recuit_job->slug }}.html">{{ $recuit_job->job_title }}</a><span class="label label-danger">GẤP</span></h4></div>
                                <p><i class="fa fa-address-book" aria-hidden="true"></i><a href="/{{ $recuit_job->province->slug }}/{{ $recuit_job->company->slug }}">{{ $recuit_job->company->name }}</a></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $recuit_job->province->name }}" style="position: relative"><i class="fa fa-map-marker" aria-hidden="true"></i><a href="/{{ $recuit_job->province->slug }}">{{ $recuit_job->province->name }}</a></span></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $recuit_job->jobWage ? $recuit_job->jobWage->name : number_format($recuit_job->job_wage) }}" style="position: relative"><i class="fa fa-usd" aria-hidden="true"></i>{{ $recuit_job->jobWage ? $recuit_job->jobWage->name : number_format($recuit_job->job_wage) }}</span></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $recuit_job->jobExperience->name }}" style="position: relative"><i class="fa fa-user-secret" aria-hidden="true"></i>{{ $recuit_job->jobExperience->name }}</span></p>
                                <p class="block-50"><span  data-toggle="tooltip" title="{{ $recuit_job->subJobEndedAt() ? $recuit_job->subJobEndedAt() : 'Hết hạn' }}" style="position: relative"><i class="fa fa-stop-circle-o" aria-hidden="true"></i></i>Còn <font color="red">{{ $recuit_job->subJobEndedAt() ? $recuit_job->subJobEndedAt() : '0' }}</font> ngày</span></p>
                                <p>{!! str_limit($recuit_job->description, 150) !!}</p>
                                <a href="/{{ $recuit_job->province->slug }}/{{ $recuit_job->industrialZone->slug }}/{{ $recuit_job->slug }}.html" class="more">Xem chi tiết</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection