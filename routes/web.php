<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::singularResourceParameters();
Route::pattern('id', '[0-9]+');
Route::pattern('slug', '[a-z0-9\-]+');
Route::pattern('slug1', '[a-z0-9\-]+');
Route::pattern('slug2', '[a-z0-9\-]+');
Route::pattern('slug3', '[a-z0-9\-]+');


//------------------ Trang quản trị (CMS) ------------------//
Route::group(['middleware' => 'admin'], function()
{
	Route::group(['prefix' => 'admin'], function() {
		Route::get('/', function(){
			return 'admin';
		});
	});
});

//------------------ Trang người dùng ------------------//
//Trang chủ
Route::get('/','HomeController@index');

//Trang dành cho ứng viên (khách không truy cập được)
Route::group(['middleware' => 'auth'], function()
{
	//Việc làm đã lưu
	Route::get('/viec-lam-da-luu', 'UserCustom\SaveJobController@index');
	//Xóa việc làm
	Route::post('/viec-lam-da-luu/xoa', 'UserCustom\SaveJobController@destroy');

	//Sửa thông tin cá nhân
	Route::get('/thong-tin-ca-nhan', 'UserCustom\ProfileController@edit');
	Route::post('/thong-tin-ca-nhan', 'UserCustom\ProfileController@update');

	//Đổi mật khẩu
	Route::get('/doi-mat-khau', 'UserCustom\ChangePasswordController@edit');
	Route::post('/doi-mat-khau', 'UserCustom\ChangePasswordController@update');

});

//Lưu việc làm (ajax)
Route::get('/luu-viec-lam/{id}', 'UserCustom\SaveJobController@store');

//Đăng xuất
Route::get('logout', function(){
	Auth::logout();
	return redirect('/');
});
//Đăng nhập, đăng ký
Auth::routes();

//Đăng nhập facebook
Route::get('/facebook/redirect', 'Auth\SocialController@redirectToProvider');
Route::get('/facebook/callback', 'Auth\SocialController@handleProviderCallback');

//Trang đăng ký nhận bản tin qua email
Route::get('/dang-ky','SubscribeController@create');
Route::post('/dang-ky','SubscribeController@store');
Route::get('/dang-ky/thong-bao.html', 'SubscribeController@success');

//Tìm kiếm
Route::get('/tim-kiem','SearchController@index');
Route::get('/tim-kiem/ajax/{id}','SearchController@loadIndustrialZone');

//Việc làm theo ngành nghề
Route::get('/nganh-nghe', 'CarrerController@category');
Route::get('/nganh-nghe/{slug1}', 'CarrerController@index');

//Kiểm trang url là Page hay trang việc làm theo tỉnh thành
Route::get('/{slug1}', 'Slug1Controller@index');

//Kiểm tra url là trang công ty hay trang khu công nghiệp
Route::get('/{slug1}/{slug2}', 'Slug2Controller@index');

//Chi tiết việc làm
Route::get('/{slug1}/{slug2}/{slug3}.html', 'JobController@index');
